**Note**: Demo images in *Categories -> Holiday*

# Folder structure

  

- **aws**
  - beforeintsall.sh
  - start.sh
  - stop.sh
- **public**
  - **css**
  - **fonts**
  - **images**
  - *favicon.ico*
  - *index.html*
  - *manifest.json*
- **src**
  - **actions**
      - *auth.js*
      - *cart.js*
      - *collectionsGallery.js*
      - *gallery.js*
      - *messaging.js*
      - *product.js*
      - *scroll.js*
  - **assets**
      -  **fonts**
      -  **images**
  - **components**
      - **Components**
          - **AuthGroup**
              - *AuthGroup.js*
          - **Cart**
              - **css**
              - *Cart.js* 
              - *CartContainer.js*
          - **Categories**
              - **css**
              - *Categories.js*
              - *Category.js*
          - **CollectionsSlider**
              - **css**
              - *Slider.js*
              - *NextArrow.js*
              - *PrevArrow.js*
              - *CollectionsSliderContainer.js*
          - **EmailSharePopup**
              - **css**
              - *EmailSharePopup.js*
              - *EmailSharePopupContainer.js*
          - **Footer**
              - **css**
              - *Footer.js*
              - *FooterContainer.js*
          - **Gallery**
              - **css**
              - *Gallery.js*
              - *Loader.js*
          - **Header**
              - **css**
              - *Header.js*
              - *HeaderContainer.js*
              - *MobileMenu.js*
              - *MobileSearch.js*
          - **ImagePreloader**
              - **css**
              - *ImagePreloader.js*
          - **Interest**
              - **css**
              - *Interest.js*
          - **LoginPopup**
              - **css**
              - *LoginPopup.js*
              - *LoginPopupContainer.js*
          - **Messaging**
              - **css**
              - *Messaging.js*
              - *MessagingContainer.js*
          - **ProductSlider**
              - **css**
              - *Gallery.js*
          - **SignupPopup**
              - **css**
              - *SignupPopup.js*
              - *SignupPopup.js*
          - **Sorting**
              - **css**
              - *Sorting.js*
              - *CheckBox.js*
          - **ToggleView**
              - **css**
              - *ToggleView.js*
          - *index.js*
      - **Pages**
          - **Home**
                - **css**
                - *Home.js*
                - *HomeContainer.js*
          - **NotFound**
                - **css**
                - *NotFound.js*
                - *NotFoundContainer.js*
          - **Product**
                - **css**
                - *Product.js*
                - *ProductContainer.js*
          - *index.js*
      - *index.js*
  - **constants**
      - *auth.js*
      - *cart.js*
      - *collectionsGallery.js*
      - *gallery.js*
      - *messaging.js*
      - *product.js*
      - *scroll.js*
  - **reducers**
      - *auth.js*
      - *cart.js*
      - *collectionsGallery.js*
      - *gallery.js*
      - *messaging.js*
      - *product.js*
      - *scroll.js*
      - *root*
- .gitignore
- appspec.yml
- codedeploy_deploy_dev.py
- codedeploy_deploy.py
- package-lock.json
- package.json
- README.md


---
### **The component structure on the Home page:**

- Container
  - Header
        - CollectionsSlider
        - Categories
        - MobileMenu
        - MobileSearch
  - Home
        - ToggleView 
        - Sorting
        - Gallery 
            - ImageTemplate
            - Loader
  - Product
  - Cart
  - EmailSharePopup
  - MessagingGlobal
  - LoginPopup
  - SignupPopup
  - Footer

---
#### The list of plugins, that we used on the Home page (node modules):  

1.  react    
2.  react-router-dom    
3.  react-redux    
4.  redux-thunk
5.  redux-devtools-extension    
6.  react-masonry-component  
7.  react-infinite-scroller

---
#### The stack of technology that we used:
 
- HTML5
- CSS3
- JavaScript
- React\Redux

### Simple example how Redux works
Let's look at an example of "toggling the cart"

![Example](scheme.png)
import categories from './fake_info/categories.json';
import imgs from './fake_info/imgs.json';

class StockThatRocksAPI {

  // get sessions
  getSessions() {
    return Promise.resolve([])
  }
  // get collections
  getCollections() {
    return Promise.resolve([])
  }
  // get categories
  getCategories() {
    return Promise.resolve(categories)
  }

  getScroll() {
    return Promise.resolve(imgs)
  }

  getSession() {
    return Promise.resolve(imgs)
  }

  getCollection() {
    return Promise.resolve([])
  }

  getProduct(id) {
    let product = imgs.products.filter(item => item.id === id)
    return Promise.resolve(product[0])
  }
}

export default new StockThatRocksAPI()

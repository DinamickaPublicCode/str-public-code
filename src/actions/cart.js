import cartConstants from '../constants/cart';
import API from '../api';

import {
  displayMessage
} from './messaging';
import {
  changeProductSource
} from './product';

const updateCart = (payload) => ({
  type: cartConstants.UPDATE_CART,
  payload
});

const saveCartState = (payload) => ({
  type: cartConstants.TOGGLE_CART,
  payload
});

const saveCartItemCount = (payload) => ({
  type: cartConstants.SAVE_CART_ITEM_COUNT,
  payload
});

export const updateCartItemCount = (cart) => {
  return (dispatch) => {
    if (!cart) {
      dispatch(saveCartItemCount(null));
      return;
    }

    let cartLength = 0;

    if (cart.membership) cartLength++;
    if (cart.packages && cart.packages.length) cartLength += cart.packages.length;
    if (cart.products && cart.products.length) cartLength += cart.products.length;

    dispatch(saveCartItemCount(cartLength));
  };
};



export const getCart = (id, shouldUpdateCart) => {
  return (dispatch) => {
    API.getCart(id)
      .then(res => {
        if (shouldUpdateCart && res) {
          dispatch(displayMessage({type: 'success', text: 'Cart updated!'}));
          dispatch(updateCartItemCount(res));
          dispatch(updateCart(res));
        }
      })
      .catch(err => {
        console.log('Failed to get cart by id ', id);
      })
  };
};

export const removeFromCart = (data, guest = false, delId) => {
  return (dispatch, getState) => {    
    dispatch(updateCartContent(data, guest));
    let { product } = getState();

    if (product.get('shown') && product.get('product')) {
      let shownProductId = product.get('product')['id'];
      if (shownProductId === delId) {
        dispatch(changeProductSource(false, 0));
      }
    }      
  };
};

export const getGuestCart = (data, noMessage = false) => {
  return (dispatch) => {
    if (!noMessage) dispatch(displayMessage({type: 'success', text: 'Cart updated!'}));
    dispatch(updateCartItemCount(data));
    dispatch(updateCart(data));
    if (window.localStorage) {
      window.localStorage.setItem('cart', JSON.stringify(data));
    }      
  };
};

export const updateCartContent = (data, guest = false, noMessage = false) => {
  return (dispatch) => {    
    if (data) {
      let totalPrice = data.products
                              .map(el => el.prices.filter(item => el.size === item.type)[0].price)
                              .reduce((prev, cur) => prev + cur, 0);
      let newData = {...data, totalPrice}
      if (!guest) {
          if (!noMessage) dispatch(displayMessage({type: 'success', text: 'Cart updated!'}));
          dispatch(updateCartItemCount(newData));
          dispatch(updateCart(newData));
      } else {
        if (!noMessage) dispatch(displayMessage({type: 'success', text: 'Cart updated!'}));
        dispatch(updateCartItemCount(newData));
        dispatch(updateCart(newData));
        if (window.localStorage) {
          window.localStorage.setItem('cart', JSON.stringify(newData));
        }
      }
    }      
  };
};

export const toggleCart = (data) => {
  return (dispatch) => {
    dispatch(saveCartState(data));
  };
};

export const createAnonymousCart = (useStorage = false) => {
  return (dispatch) => {
      if (window.localStorage && !useStorage) {
      if (window.localStorage.getItem('cart')) {
        dispatch(getGuestCart(JSON.parse(window.localStorage.getItem('cart')), true))
        return;
      }
    }
  };
};




import productConstants from '../constants/product';
import API from '../api';

import {
  displayMessage
} from './messaging';

const updateSelectedProduct = (payload) => ({
  type: productConstants.UPDATE_SELECTED_PRODUCT,
  payload
});

const updateSelectedProductSession = (payload) => ({
  type: productConstants.UPDATE_SELECTED_PRODUCT_SESSION,
  payload
})

const setProductPrices = (payload) => ({
  type: productConstants.UPDATE_PRODUCT_PRICES,
  payload
})

const toggleShowProduct = (payload) => ({
  type: productConstants.TOGGLE_PRODUCT,
  payload
})

const saveProductId = (payload) => ({
  type: productConstants.SAVE_PRODUCT_ID,
  payload
})

const saveProductSource = (payload) => ({
  type: productConstants.CHANGE_SOURCE,
  payload
})

const saveCartIndex = (payload) => ({
  type: productConstants.SET_CART_INDEX,
  payload
})

const saveSelectedPrice = (payload) => ({
  type: productConstants.SAVE_SELECTED_PRICE,
  payload
})

const saveSharePopupState = (payload) => ({
  type: productConstants.SAVE_SHARE_POPUP_STATE,
  payload
})

export const clearProduct = (payload) => ({
  type: productConstants.SAVE_PRODUCT_ID,
  payload
});

export const changeProductSource = (data, index) => {
  return (dispatch) => {
    dispatch(setCartIndex(index))
    dispatch(saveProductSource(data));
  };
};

export const selectProductId = (id, index = null) => {
  return (dispatch) => {
    if (index) dispatch(setCartIndex(index));
    dispatch(saveProductId(id));
  };
};

export const setCartIndex = (index) => {
  return (dispatch) => {
    dispatch(saveCartIndex(index));
  };
};

export const getProductFromCart = (data, id) => {
  return (dispatch) => {

    API.getProduct(id)
      .then(res => {
        if (data.prices && data.prices.length) {
          if (data.prices[0]) {
            dispatch(saveSelectedPrice(data.prices[0]['type']));
          }
        }
        if (res.prices && res.prices.length ) {
            let prices = {};
            res.prices.forEach((el, i) => {
            prices[el.type] = el.price;
            })
          dispatch(setProductPrices(prices));
        }

        dispatch(updateSelectedProduct(data));
        if (data.userSessionId) {
           dispatch(getProductSession(data.userSessionId));
        }
      })


  };
};

export const toggleProductVisibility = (data) => {
  return (dispatch) => {
    if (data) document.body.style.overflowY = 'hidden';
    if (!data) document.body.style.overflowY = 'visible';
    dispatch(toggleShowProduct(data));
  };
};

export const getProductSession = (id) => {
  return (dispatch) => {
    API.getSession(id)
    .then((res) => {
      res['pageId'] = Math.random();
      dispatch(updateSelectedProductSession(res));
    })
    .catch(err => {
      console.log('Failed to get product session by id ', id);
    });
  };
};

export const selectProduct = (id) => {
  return (dispatch) => {
    
    // clear product selection
    if (!id) {
      dispatch(updateSelectedProduct(null));
      dispatch(saveProductId(null));
      return;
    }

    API.getProduct(id)
      .then(res => {
        if (!res) throw new Error('failed to fetch product');
        dispatch(updateSelectedProduct(res));
        if (res.userSessionId) {
           dispatch(getProductSession(res.userSessionId));
        }
        if (res.prices && res.prices.length ) {
            let prices = {};
            res.prices.forEach((el, i) => {
            prices[el.type] = el.price;
            })
          dispatch(setProductPrices(prices));
        }
      })
      .catch(err => {
        console.log(err);
        dispatch(displayMessage({ type: 'error', text: 'Failed to fetch product!' }));
        dispatch(toggleProductVisibility(false));
        console.log('Failed to get product by id ', id);
      });
  };
};

export const toggleEmailSharePopup = (data) => {
  return (dispatch) => {
    dispatch(saveSharePopupState(data))
  };
};

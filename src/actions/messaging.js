import messagingConstants from '../constants/messaging';

const updateMessage = (payload) => ({
  type: messagingConstants.UPDATE_MESSAGE,
  payload
});

export const displayMessage = (data) => {
  return (dispatch) => {
    dispatch(updateMessage(data));
  };
};



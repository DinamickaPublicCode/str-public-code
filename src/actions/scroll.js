import scrollConstants from '../constants/scroll';

const saveScrollPosition = (payload) => ({
  type: scrollConstants.SAVE_SCROLL_POSITION,
  payload
})

export const setScrollPosition = (data) => {
  return (dispatch) => {
    dispatch(saveScrollPosition(data));
  };
}; 

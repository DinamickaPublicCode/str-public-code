import collectionsGalleryConstants from '../constants/collectionsGallery';
import API from '../api';

const setCollections = (payload) => ({
  type: collectionsGalleryConstants.UPDATE_COLLECTIONS,
  payload
});

const setSessions = (payload) => ({
  type: collectionsGalleryConstants.UPDATE_SESSIONS,
  payload
});

const setCategories = (payload) => ({
  type: collectionsGalleryConstants.UPDATE_CATEGORIES,
  payload
});

export const getCollections = () => {
  return (dispatch) => {
    API.getCollections()
      .then(res => {
        if (res) {
          dispatch(setCollections(res));
        }
        if (!res || res.length === 0) {
          dispatch(setEmptyGallery(true))
        }
      })
      .catch(err => console.log('Failed to get collections', err))
  };
};

export const getSessions = () => {
  return (dispatch) => {
    API.getSessions()
    .then(res => {
      if (res) {
        dispatch(setSessions(res));
      }
      if (!res || res.length === 0) {
        dispatch(setEmptyGallery(true))
      }
    })
    .catch(err => console.log('Failed to get sessions', err))
  };
};

export const getCategories = () => {
  return (dispatch) => {
    API.getCategories()
    .then(res => {
      if (res) {
        dispatch(setCategories(res));
      }
      if (!res || res.length === 0) {
        dispatch(setEmptyGallery(true))
      }
    })
    .catch(err => console.log('Failed to get categories', err))
  };
};

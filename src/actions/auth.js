import authConstants from '../constants/auth';

const saveAuthMessage = (payload) => ({
  type: authConstants.UPDATE_MESSAGE,
  payload
})

const saveLoginPopupState = (payload) => ({
  type: authConstants.TOGGLE_LOGIN_POPUP,
  payload
})

const saveSignupPopupState = (payload) => ({
  type: authConstants.TOGGLE_SIGNUP_POPUP,
  payload
})

export const toggleLoginPopup = (data) => {
  return (dispatch) => {
    dispatch(saveLoginPopupState(data));
  };
};

export const toggleSignupPopup = (data) => {
  return (dispatch) => {
    dispatch(saveSignupPopupState(data));
  };
};

export const updateAuthMessage = (data) => {
  return (dispatch) => {
    dispatch(saveAuthMessage(data));
  };
};




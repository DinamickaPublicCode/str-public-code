import galleryConstants from '../constants/gallery';
import API from '../api';

const saveGallery = (payload) => ({
	type: galleryConstants.UPDATE_GALLERY,
	payload
})

const saveTotalProducts = (payload) => ({
	type: galleryConstants.UPDATE_TOTAL_PRODUCTS,
	payload
})

const saveSeardId = (payload) => ({
	type: galleryConstants.SAVE_SEARCH_ID,
	payload
})
const saveSeardPosition = (payload) => ({
	type: galleryConstants.SAVE_SEARCH_POSITION,
	payload
})
const saveToggleNoProducts = (payload) => ({
	type: galleryConstants.TOGGLE_NO_PRODUCTS,
	payload
})

const updateSearchSubject = (payload) => ({
	type: galleryConstants.UPDATE_SEARCH_SUBJECT,
	payload
})

const updateSearchParams = (payload) => ({
	type: galleryConstants.UPDATE_SEARCH_PARAMS,
	payload
})

export const setSearchParams = (data) => {
	return (dispatch) => {
		dispatch(updateSearchParams(data));
	}
}

export const setSearchSubject = (data) => {
	return (dispatch) => {
		dispatch(updateSearchSubject(data));
	};
};


export const toggleNoProducts = (data) => {
	return (dispatch) => {
		dispatch(saveToggleNoProducts(data));
	};
};

export const updateGallery = (data = null) => {
	return (dispatch, getState) => {
		if (!data) {
			console.log('if')
			let { gallery } = getState();
			data = gallery.get('gallery')['data'];
		}
		dispatch(saveGallery({ id: Math.random, data: data }));
	};
};

export const getFeaturedProducts = (params, gallery, searchId, searchPosition) => {
	return (dispatch) => {
		dispatch(getGalleryProducts(params, gallery, searchId, searchPosition));
	};
};

export const updateSearchId = (data) => {
	return (dispatch) => {
		dispatch(saveSeardId(data));
	};
};
export const updateSeardPosition = (data) => {
	return (dispatch) => {
		dispatch(saveSeardPosition(data));
	};
};
export const updateTotalProducts = (data) => {
	return (dispatch) => {
		dispatch(saveTotalProducts(data));
	};
};

export const getCollectionProducts = (id, params, gallery, searchId, searchPosition) => {
	return (dispatch) => {
		if (!id) return;
		API.getCollection(id)
			.then((res) => {
				if (!res) throw new Error();
				let col_data = {
					collections: [res],
					timePeriod: "ALL"
				}
				let reqdata = Object.assign({}, params, col_data);
				dispatch(getGalleryProducts(reqdata, gallery, searchId, searchPosition));
			})
			.catch(err => console.log('Failed to fetch category for gallery ', err));
	};
};

export const getCategoryProducts = (id, params, gallery, searchId, searchPosition, url) => {
	return (dispatch) => {
		if (!id) return;
		dispatch(getGalleryProducts(null, gallery, searchId, searchPosition, url));
	};
};

export const getSessionProducts = (id, params, gallery, searchId, searchPosition) => {
	return (dispatch) => {
		if (!id) return;
		let s_data = {
			sessionId: id,
			timePeriod: "ALL"
		}
		let reqdata = Object.assign({}, params, s_data);
		dispatch(getGalleryProducts(reqdata, gallery, searchId, searchPosition));
	};
};


export const getGalleryProducts = (data, gallery = [], searchId, searchPosition, url) => {
	return (dispatch) => {
		API.getScroll()
			.then(res => {
				if (url === "/categories/1600636900979245056") {
					if (!res) dispatch(toggleNoProducts(true));
					let products = res['products'];
					// eslint-disable-next-line
					Array.prototype.unique = function () {
						var a = this.concat();
						for (var i = 0; i < a.length; ++i) {
							for (var j = i + 1; j < a.length; ++j) {
								if (a[i].id === a[j].id)
									a.splice(j--, 1);
							}
						}
						return a;
					};
					let uniqqe = gallery.data.concat(products).unique();
					if (products.length === 0) {
						dispatch(toggleNoProducts(true));
						return;
					}
					dispatch(saveSeardPosition(uniqqe.length))
					if (!searchId) dispatch(saveTotalProducts(res['totalProducts']));
					if (!searchId) dispatch(saveSeardId(res['searchId']));
					dispatch(updateGallery(uniqqe));
				} else {
					dispatch(toggleNoProducts(true));
				}

			})
			.catch(err => {
				console.log('FAILED TO FETCH GALLERY PRODUCTS', err);
			})
	};
};
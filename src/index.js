import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Container from './Container';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'infinite-scroll/dist/infinite-scroll.pkgd.js';
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'


ReactDOM.render(<Container />, document.getElementById('root'));

import { Map } from 'immutable';
import authConstants from '../constants/auth';

const initialState = Map({
  loggedIn: null,
  user: null,
  authMessage: { type: 'error', text: '' },
  loginPopupShown: false,
  signupPopupShown: false,
  guestPopupShown: false,
  isInit: false,
  pendingAccConfirm: false,
  agencyUser: false
})

export default function (state = initialState, action) {
  switch (action.type) {
    case authConstants.UPDATE_MESSAGE:
      return state.set('authMessage', action.payload);
    case authConstants.TOGGLE_LOGIN_POPUP:
      return state.set('loginPopupShown', action.payload);
    case authConstants.TOGGLE_SIGNUP_POPUP:
      return state.set('signupPopupShown', action.payload);
    default:
      return state;
  }
}

import { Map } from 'immutable';
import cartConstants from '../constants/cart';

const initialState = Map({
  cart: {
    id: null,
    products: [],
    totalPrice: 0
  },
  open: false,
  cartLength: 0,
  purchaseHistory: [],
  order: null,
  boughtCart: null,
  pendingPackage: null,
  pendingMembership: null,
  purchaseHistoryTotal: 0,
  freezeErrors: {}
});

export default function (state = initialState, action) {
  switch(action.type) {
    case cartConstants.UPDATE_CART:
      return state.set('cart', action.payload);
    case cartConstants.TOGGLE_CART:
      return state.set('open', action.payload);
    case cartConstants.SAVE_CART_ITEM_COUNT:
      return state.set('cartLength', action.payload);
    default:
      return state;
  }
};

import { Map } from 'immutable';
import messagingConstants from '../constants/messaging';

const initialState = Map({
  message: { type: 'error', text: '' },
  confirmPopupShown: false,
  token: null,
  confirmationPopupText: null,
  confirmationPopupFunc: null,
  confirmationPopupArg: null,
  infoPopupShown: false,
  infoPopupText: ""
});

export default function(state = initialState, action) {
  switch(action.type) {
    case messagingConstants.UPDATE_MESSAGE:
      return state.set('message', action.payload);
    default:
      return state;
   }
}

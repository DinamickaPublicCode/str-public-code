import { Map } from 'immutable';
import galleryConstants from '../constants/gallery';

const initialState = Map({
    gallery: { id: 0, data: [] },
    totalProducts: null,
    searchId: null,
    searchPosition: null,
    noProducts: false,
    collections: { id: 0, data: [] },
    sessions: { id: 0, data: [] },
    categories: { id: 0, data: [] },
    searchSubject: "",
    searchParams: null 
});

export default function(state = initialState, action) {
    switch (action.type) {
        case galleryConstants.UPDATE_GALLERY:
            return state.set('gallery', action.payload);
        case galleryConstants.UPDATE_TOTAL_PRODUCTS:
            return state.set('totalProducts', action.payload);
        case galleryConstants.SAVE_SEARCH_ID:
            return state.set('searchId', action.payload);
        case galleryConstants.SAVE_SEARCH_POSITION:
            return state.set('searchPosition', action.payload);
        case galleryConstants.TOGGLE_NO_PRODUCTS:
            return state.set('noProducts', action.payload);
        case galleryConstants.UPDATE_SEARCH_SUBJECT:
            return state.set('searchSubject', action.payload);
        case galleryConstants.UPDATE_SEARCH_PARAMS:
            return state.set('searchParams', action.payload);
        default:
            return state;
    }
};

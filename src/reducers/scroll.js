import { Map } from 'immutable';
import scrollConstants from '../constants/scroll';

const initialState = Map({
  scrollPosition: 0
});

export default function(state = initialState, action) {
  switch(action.type) {
    case scrollConstants.SAVE_SCROLL_POSITION:
      return state.set('scrollPosition', action.payload);
    default:
      return state;
  }
}; 

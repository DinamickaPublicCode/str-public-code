import { Map } from 'immutable';
import productConstants from '../constants/product';

const initialState = Map({
  product: null,
  session: null,
  prices: {},
  id: null,
  shown: false,
  fromCart: false,
  cartIndex: 0,
  selectedPrice: null,
  emailShareShown: false
});

export default function(state = initialState, action) {
  switch(action.type) {
    case productConstants.UPDATE_SELECTED_PRODUCT:
      return state.set('product', action.payload);
    case productConstants.UPDATE_SELECTED_PRODUCT_SESSION :
      return state.set('session', action.payload);
    case productConstants.UPDATE_PRODUCT_PRICES:
      return state.set('prices', action.payload);
    case productConstants.TOGGLE_PRODUCT:
      return state.set('shown', action.payload);
    case productConstants.SAVE_PRODUCT_ID:
      return state.set('id', action.payload);
    case productConstants.CHANGE_SOURCE:
      return state.set('fromCart', action.payload);
    case productConstants.SET_CART_INDEX:
      return state.set('cartIndex', action.payload);
    case productConstants.SAVE_SELECTED_PRICE:
      return state.set('selectedPrice', action.payload);
    case productConstants.SAVE_SHARE_POPUP_STATE:
      return state.set('emailShareShown', action.payload);
    default:
      return state;
  }
};

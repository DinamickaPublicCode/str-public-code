import { Map } from 'immutable';
import collectionsGalleryConstants from '../constants/collectionsGallery';

const initialState = Map({
  collections: [],
  sessions: [],
  categories: [],
  empty: false
});

export default function(state = initialState, action) {
  switch(action.type) {
    case collectionsGalleryConstants.UPDATE_SESSIONS:
      return state.set('sessions', action.payload);
    case collectionsGalleryConstants.UPDATE_COLLECTIONS:
      return state.set('collections', action.payload);
    case collectionsGalleryConstants.UPDATE_CATEGORIES:
      return state.set('categories', action.payload);
    default:
      return state;
  }
}

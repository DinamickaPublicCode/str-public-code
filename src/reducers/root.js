import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import auth from './auth';
import product from './product';
import messaging from './messaging';
import cart from './cart';
import gallery from './gallery';
import scroll from './scroll';
import collectionsGallery from './collectionsGallery';

export default combineReducers({
  auth,
  product,
  messaging,
  cart,
  gallery,
  scroll,
  collectionsGallery,
  router: routerReducer
})

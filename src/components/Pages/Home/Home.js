import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';

import './css/Home.css';

import {
  ToggleView,
  Gallery,
  Sorting
} from '../../Components'

class Home extends Component {
  constructor(props) {
    super(props)

      let defaultFilter = null;
      let path = props.history.location.pathname;
      if (path.includes('/collections') || path.includes('/sessions')) {
        defaultFilter = 'Latest'
      } else if (path.includes('/categories') || path === '/') {
        defaultFilter = 'Popular'
      }

    this.state = {
      topFilter: "THIS_MONTH",
      leftFilters: [],
      categoryImageToDisplay: [],
      MoreGallery: false,
      defaultFilter: defaultFilter,
      galleryPreloader: true,
      fetching: false
    }

    this.topFilterChange = this.topFilterChange.bind(this);
  }

  componentDidMount() {
    window.scrollTo(0,0);
    window.$('#root').css({"overflow": "visible", "position": "static", "marginTop": 0});
  }
  componentWillUnmount(){
    if (this.props.search) { //clear search if user leave search
      this.props.setSearchSubject('');
      this.props.setSearchParams(null);
    }
  }
  getProducts() {
    let path = this.props.history.location.pathname;
    let {
      getFeaturedProducts,
      getCollectionProducts,
      getCategoryProducts,
      getSessionProducts,
      gallery,
      searchId = null,
      searchPosition = null
    } = this.props;


    let params = {
      filters:this.state.leftFilters,
      timePeriod: window.innerWidth > 767 ? this.state.topFilter : "ALL" //see ALL images on mobile
    }

    if (searchPosition) {
      params.searchPosition = searchPosition;
    }
    if (this.props.search){ //add param if user doing search
      params.query = this.props.searchSubject.trim();
      params.timePeriod = "ALL";

      if (this.props.searchParams) {
        if (this.props.searchParams.category) {
          params.categories = [this.props.searchParams.category];
        }
        if (this.props.searchParams.sessionId) {
          params.sessionId = this.props.searchParams.sessionId;
        }
        if (this.props.searchParams.collection) {
          params.collections = [this.props.searchParams.collection];
        }
      }

    }

    if (path.includes('/categories')) {
      // CATEGORIES
      let cat_id = this.props.match.params.id;
      let url = this.props.match.url;
      getCategoryProducts(cat_id, params, gallery, searchId, undefined, url);
    }
    else if (path.includes('/collections')) {
      // COLLECTIONS
      let col_id = this.props.match.params.id;
      getCollectionProducts(col_id, params, gallery, searchId);

    }
    else if (path.includes('/sessions')) {
      // SESSIONS
      let s_id = this.props.match.params.id;
      getSessionProducts(s_id, params, gallery, searchId,searchPosition);
    } else {
      params.featured = true;
      if (this.props.search) delete params.featured;
      getFeaturedProducts(params, gallery, searchId);
    }

  }


  topFilterChange = (currentFilter) => {
    let {
      updateGallery,
      updateSearchId,
      updateTotalProducts,
      updateSeardPosition
     } = this.props;

    this.setState({
      topFilter: currentFilter,
      MoreGallery: true,
      galleryPreloader: true
    });

    updateSearchId(null);
    updateSeardPosition(null);
    updateTotalProducts(null);
    updateGallery([]);
  }

  leftFilterChange = (currentFilters) => {
    let {
      updateGallery,
      updateSearchId,
      updateTotalProducts,
      updateSeardPosition
     } = this.props;

    this.setState({
      leftFilters: currentFilters,
      MoreGallery:true,
      galleryPreloader: true
    });

    updateSearchId(null);
    updateSeardPosition(null);
    updateTotalProducts(null);
    updateGallery([]);
  }

  uploadMore = () => {
    if (this.state.fetching) return;
    setTimeout(() => {
      this.setState({ fetching: false })
    }, 800)
    this.setState({ fetching: true }, () => {
        this.getProducts()
    })

  }

  addPictureToAlbum = (product) => {
    if (!this.props.user) {
      let { toggleLoginPopup } = this.props;
      toggleLoginPopup(true);
      return;
    }
  }

  toggleDownloadPopup = (val, product) => {
    let {
      user
     } = this.props;

    if (!user) {
      let { toggleLoginPopup } = this.props;
      toggleLoginPopup(true);
      return;
    }

    let type = null;
    if (user && user.user && user.user.package) {
      if ( user.user.package.webSizePhotosLeft > 0) type = 'web';
      else if ( user.user.package.printSizePhotosLeft > 0) type = 'print';
    }
    if (user && user.user && user.user.activeMembership) {
      if ( user.user.activeMembership.webSizePhotosLeft > 0) type = 'web';
      else if ( user.user.activeMembership.printSizePhotosLeft > 0) type = 'print';
    }

    if (!type) type = (product.size === 'ALL' || product.size === 'WEB') ? 'WEB' : 'PRINT';

  }

  toggleLoginPopup = (message) => {
    let { toggleLoginPopup } = this.props;
    toggleLoginPopup(true);
  }

  updateFavorites = (type, val, toFaves) => {
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.router !== this.props.router) {
      let {
        updateGallery,
        updateSearchId,
        updateSeardPosition,
        updateTotalProducts,
        toggleNoProducts
       } = this.props;

      updateSearchId(null);
      updateSeardPosition(null);
      updateTotalProducts(null);
      updateGallery([]);
      toggleNoProducts(false);

      this.setState({
        MoreGallery: true,
        galleryPreloader: true
      })
      window.scrollTo(0,0);
    }


    if (this.props.gallery !== nextProps.gallery) {
      if (nextProps.totalProducts) {
        if (nextProps.gallery.data.length >= nextProps.totalProducts) {
          this.setState({ MoreGallery: false });
        }
      }
    }

    if (this.props.noProducts !== nextProps.noProducts && nextProps.noProducts) {

      this.setState({ MoreGallery: false, galleryPreloader: false });
      this.props.toggleNoProducts(false);
    }
  }

  updateCart = (cart, id, fave) => {
    if (!cart.products) return;
    let inCart = false;
    let prods = cart.products.map((el, i) => {
      if (el.id === id) {
        el['inFavorites'] = fave;
        inCart = true;
      }
      return el;
    })

    if (!inCart) return;
    let newCart = Object.assign({}, cart);
    newCart['products'] = prods;
    this.props.updateCartContent(newCart, false, true);
  }

  getFavorites = () => {
    let { getFavorites } = this.props;
    getFavorites();
  }

  render() {
    let {topFilter, defaultFilter, data} = this.state
    let path = this.props.history.location.pathname;
    let type = "product"

    let toggleHidden = true;
    if (path === '/') toggleHidden = false

    let {
      selectProductId,
      toggleProductVisibility,
      downloadInfoShown,
      changeProductSource,
      updateGallery,
      cart,
      gallery,
      modifiedFaves,
      loggedIn,
      scrollPosition,
      search
     } = this.props;

    return (
      <div className="home-wrapper home-gallery">

        {toggleHidden ? null : <ToggleView onClick={this.topFilterChange} filter={topFilter} />}
        <div className="container-fluid sidebar-offset" style={search ?  {paddingLeft:20} : null}>
          <div className="row" style={!toggleHidden ? null : {paddingTop: 20}}>
            <Sorting
              isSearching={search}
              onChange={this.leftFilterChange}
              offset={scrollPosition}
              leftFilters={this.state.leftFilters}
              defaultFilter={defaultFilter}
            />
            <div className="container-fluid">
              <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <Gallery
                      downloadIndoShown={downloadInfoShown}
                      key="gallery"
                      originalData={data}
                      searching={true}
                      type={type}
                      getFavorites={this.getFavorites}
                      imageClick={(id) => {
                        changeProductSource(false);
                        selectProductId(id);
                        toggleProductVisibility(true);
                        return false

                      }}
                      loggedIn={loggedIn}
                      updateCart={this.updateCart}
                      cart={cart}
                      toggleDownloadPopup={(val, el) => this.toggleDownloadPopup(val, el)}
                      addToAlbum={this.addPictureToAlbum}
                      updateGallery={updateGallery}
                      images={gallery.data}
                      updateFavorites={this.updateFavorites}
                      toggleLoginPopup={this.toggleLoginPopup}
                      hasMore={this.state.MoreGallery}
                      OnLoadMore={this.uploadMore}
                      modifiedFaves={modifiedFaves}
                      preloader={this.state.galleryPreloader} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Home);

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Home from './Home';

import {
    selectProductId,
    toggleProductVisibility,
    changeProductSource
} from '../../../actions/product';

import {
    getCart,
    updateCartContent
} from '../../../actions/cart';

import {
    displayMessage
} from '../../../actions/messaging';

import {
    updateGallery,
    getFeaturedProducts,
    getCollectionProducts,
    getCategoryProducts,
    getSessionProducts,
    updateTotalProducts,
    updateSearchId,
    updateSeardPosition,
    toggleNoProducts,
    setSearchSubject,
    setSearchParams
} from '../../../actions/gallery';

import {
    toggleLoginPopup
} from '../../../actions/auth';


const mapStateToProps = ({ auth, product, gallery, router, cart, scroll }) => ({
    loggedIn: auth.get('loggedIn'),
    user: auth.get('user'),
    productShown: product.get('shown'),
    gallery: gallery.get('gallery'),
    searchSubject: gallery.get('searchSubject'),
    totalProducts: gallery.get('totalProducts'),
    searchId: gallery.get('searchId'),
    searchPosition:gallery.get('searchPosition'),
    noProducts: gallery.get('noProducts'),
    cart: cart.get('cart'),
    scrollPosition: scroll.get('scrollPosition'),
    searchParams: gallery.get('searchParams'),
    router: router
});

const matchDispatchToProps = dispatch =>
    bindActionCreators({
        selectProductId,
        toggleProductVisibility,
        changeProductSource,
        updateGallery,
        getFeaturedProducts,
        getCollectionProducts,
        getCategoryProducts,
        getSessionProducts,
        updateTotalProducts,
        updateSearchId,
        toggleNoProducts,
        getCart,updateSeardPosition,
        updateCartContent,
        displayMessage,
        toggleLoginPopup,
        setSearchSubject,
        setSearchParams
    }, dispatch)

export default connect(mapStateToProps, matchDispatchToProps)(Home);

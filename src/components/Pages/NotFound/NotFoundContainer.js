import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NotFound from './NotFound';

const mapStateToProps = ({ router }) => ({
  router: router
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({

  }, dispatch);

  export default connect(mapStateToProps, matchDispatchToProps)(NotFound);

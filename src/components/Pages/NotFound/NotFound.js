import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './css/NotFound.css';

export default class NotFound extends Component {
  render() {
    return (
      <div className="newpass-wrapper">
        <div className="forgot-password-background">
            <h2 className="forgot-password-title"><span>404 PAGE NOT FOUND</span></h2>
            <Link className="not-found-home-link" to="/"><i className="fa fa-arrow-left"></i> Back to homepage</Link>
        </div>
      </div>
    )
  }
}

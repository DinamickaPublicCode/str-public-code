import React, { Component } from 'react';
import './css/Product.css';
import API from '../../../api';
import { withRouter, Link } from 'react-router-dom';
import {
  Loader,
  ProductSlider
} from '../../Components';

const pageWidthValue = 1000;

class Product extends Component {
  state = {
    collections: null,
    w: 0,
    h: 0,
    width: 0,
    createAlbumOpen: false,
    directDownloadOpen: false,
    extChecked: false,
    freezeChecked: false,
    prices: {},
    print: false,
    web: false,
    price: 0,
    initialLoading: true,
    pageWidth: 'unset',
    isVertical: false,
    sliderLoading: false,
    printBought: false, // flag indicating if this product was bought in print size;
    webBought: false, // flag indicating if this product was bought in web size;
    loadError: false,
    imgHeight: 'unset'
  }

  disableButton = false; // disable button flag for agency user when he has no available credits of the selected type;

  handleResize = (e, isError = false) => {

    if (!this.img || !this.smImg) return;
    if (isError) this.setState({ loadError: true });

    let isVertical = this.img.width <= this.img.height;

    this.setState({
      w: !isError ? this.img.width : 280 ,
      h: window.innerHeight,
      width: window.innerWidth,
      imgHeight: this.prodInfo ? window.$(this.prodInfo).height() : 'unset',
      pageWidth: 1000, //this.img.width + 700, //(isVertical ? 700 : 800),
      isVertical: isVertical
    });
  }

  initComponent = (props) => {
    let { selectProduct, cart, id, fromCart, cartIndex, getProductFromCart, pageMode } = props;
    if (!id) {
      if (!this.props.match.params.id) {
        if (this.props.history.location.pathname.includes('/product/')) {
          id = this.props.history.location.pathname.substr(9);
        } else {
          return false;
        }
      }
      else {
        id = this.props.match.params.id;
      }
    }
    if (!id) return;

    // Get product
    if (!fromCart) {
      selectProduct(id);
    } else {
      getProductFromCart(cart['products'][cartIndex], id);
    }

    if (!pageMode) {
      this.scrollOffset = window.scrollY;
      window.$('#root').css({ "overflow": "hidden", "position": "fixed", "marginTop": -window.scrollY });
    } else {
      setTimeout(() => window.scrollTo(0, 0), 1000);
    }
  }

  populateProductOptions = (product) => {
    let { loggedIn, agencyUser } = this.props;
    this.setState({
      freezeChecked: (loggedIn && !agencyUser) ? product.isMarketFreeze : false,
      extChecked: (loggedIn && !agencyUser) ? (product.extendedLicense || product.isMarketFreeze) : false,
    })
  }

  populateSizeOptions = (selectedPrice) => {
    this.setState({
      web: false, //selectedPrice === 'WEB' ? true : false,
      print: false//selectedPrice === 'PRINT' ? true : false
    })
  }

  initFaves = (nextProps) => {
    let { updateModifiedFavorites } = nextProps;
    updateModifiedFavorites('product', nextProps.product, nextProps.product.inFavorites);
  }

  componentWillReceiveProps(nextProps) {

    if (this.props.id !== nextProps.id && nextProps.id) {
      this.setState({ initialLoading: true }, () => {
        this.initComponent(nextProps);
      })
    }

    if (this.props.product !== nextProps.product && nextProps.product) {

      if (this.state.initialLoading) {
        this.setState({ initialLoading: false });

        // set product bought status
        if (nextProps.product.isAlreadyDownload) {

          if (nextProps.product.downloadedSize === 'PRINT') {
            // print size was already bought
            this.setState({
              printBought: true,
              webBought: true
            })
          } else if (nextProps.product.downloadedSize === 'WEB') {
            // web size was already bought
            this.setState({
              webBought: true,
              printBought: false
            })

          }
        } else if (this.state.printBought || this.state.webBought) {
          // clear bought options
          this.setState({
            webBought: false,
            printBought: false
          })
        }

        //this.initFaves(nextProps);
      };

      if (this.props.product && this.props.product.id === nextProps.product.id) {
        this.initFaves(nextProps);
      }

      this.populateProductOptions(nextProps.product);
    }

    if (this.props.pageMode && this.props.match.params.id !== nextProps.match.params.id && nextProps.match.params.id) {
      this.setState({ initialLoading: true }, () => {
        this.disableButton = false;
        this.initComponent(nextProps);
      })
    }

    if (this.props.fromCart !== nextProps.fromCart) {
      this.initComponent(nextProps);
    }

    if (this.props.prices !== nextProps.prices) {
      this.calculatePrice(nextProps);
    }

    if (!this.props.user && nextProps.user && (this.props.id || nextProps.id)) {
      this.initComponent(nextProps);
    }

    if (this.props.router !== nextProps.router
      && this.props.router.location.pathname !== nextProps.router.location.pathname) {
      this.props.toggleProductVisibility(false);
    }

    if (this.props.shown && !nextProps.shown) {
      window.$('#root').css({ "overflow": "visible", "position": "static", "marginTop": 0 });
      window.scrollTo(0, this.scrollOffset || 0);
      this.setState({ web: false, print: false });
      // clear product data
      this.props.selectProduct(null);
    }
  }

  componentDidMount() {

    // TEMP: get Collections
    API.getCollections()
      .then((res => {
        if (res) {
          this.setState({ collections: res })
        }
      }))

    window.addEventListener('resize', this.handleResize);
    this.handleResize();
    if (!this.props.id && !this.props.match.params.id) return;
    if (this.props.pageMode) document.body.style.overflow = 'hidden';
    this.initComponent(this.props);

  }

  sharePin = () => {
    if (!window.PinUtils) return;
    let { product } = this.props;
    if (!product || !product.image) return;

    window.PinUtils.pinOne({
      'url': product.image.thumbUrl,
      'media': product.image.thumbUrl,
      'description': 'Stock Photo by Stock That Rocks. ' + (product.caption ? 'Keywords: ' + product.caption : '')
    });
  }

  shareEmail = () => {
    let { toggleEmailSharePopup } = this.props;
    toggleEmailSharePopup(true);
  }

  changeProduct = (id) => {
    if (this.clicked) return;
    this.clicked = true;
    setTimeout(() => { this.clicked = false }, 300);
    let { selectProductId, changeProductSource } = this.props;
    this.setState({ sliderLoading: false }, () => {
      changeProductSource(false);
      selectProductId(id);
    });
  }

  closeProductFromOverlay = (e) => {
    if (e && e.target.className.includes('product-wrapper')) {
      this.props.toggleProductVisibility(false);
    }
  }

  manageFaves = (id) => {

    if (!this.props.user) {
      let { toggleLoginPopup } = this.props;
      toggleLoginPopup(true);
      return;
    }
    let {
      product,
      user,
      updateCartContent,
      cart,
      modifiedFaves,
    } = this.props;

    if (!user || !product) return;

    let reqdata = {};
    reqdata['product'] = product;

    let inCart = false;
    let cartIndex = null;

    if (cart && cart.products) {
      for (let i = 0; i < cart.products.length; i++) {
        if (cart.products[i].id === product.id) {
          inCart = true;
          cartIndex = i;
        }
      }
    }

    let newCart = null;
    if (inCart) {
      newCart = Object.assign({}, cart);
    }

    let isFave = product.inFavorites;
    if (modifiedFaves['product'][product.id] === 1) {
      isFave = true;
    } else if (modifiedFaves['product'][product.id] === 0) {
      isFave = false;
    }

    if (isFave) {

      if (inCart && newCart) {
        newCart['products'][cartIndex]['inFavorites'] = false;
        updateCartContent(newCart, false, true);
      }

    } else {
      if (inCart && newCart) {
        newCart['products'][cartIndex]['inFavorites'] = true;
        updateCartContent(newCart, false, true);
      }
    }
  }

  toggleDirectDownload = (id) => {
    console.log('GO!')
    if (!this.props.user) {
      let { toggleLoginPopup } = this.props;
      toggleLoginPopup(true);
      return;
    }
    
    let { web, print } = this.state;
    if (!web && !print) {
      this.props.displayMessage({ type: 'error', text: 'Please, choose type of download!' });
      return;  
    };
  }

  updateCartItem = (product, index) => {
    let { cart, updateCartContent, user } = this.props;
    let { web, freezeChecked, extChecked } = this.state;

    let newCart = Object.assign({}, cart);

    let newProduct = Object.assign({}, product);
    newProduct['size'] = web ? 'WEB' : 'PRINT';

    if (freezeChecked) newProduct['isMarketFreeze'] = true;
    if (!freezeChecked) newProduct['isMarketFreeze'] = false;

    if (extChecked && !freezeChecked) newProduct['extendedLicense'] = true;
    if (!extChecked) newProduct['extendedLicense'] = false;

    newCart['products'][index] = newProduct;

    let isGuest = user ? false : true;

    // if market freeze is enabled
    if (!isGuest && user && product.isMarketFreeze && user.user.id !== product.freezeOwnerId) {
        return;
    } else if (isGuest && product && product.isMarketFreeze) {
      return;
    }

    updateCartContent(newCart, isGuest);
  }

  manageFreeze = () => {
    let { freezeChecked, extChecked } = this.state;

    if (!this.props.loggedIn) return;
    if (!extChecked && !freezeChecked) this.setState({ extChecked: true });
    if (extChecked && freezeChecked) this.setState({ extChecked: false });
    this.setState({ freezeChecked: !freezeChecked }, () => {
      // calculate price if add to cart option is active
      this.calculatePrice();
    })
  }

  manageExtLicense = () => {
    let { freezeChecked, extChecked } = this.state;

    if (freezeChecked || !this.props.loggedIn) return;
    this.setState({ extChecked: !extChecked }, () => {
      // calculate price if add to cart option is active
      this.calculatePrice();
    });
  }

  toggleSize = (type) => {
    this.setState({
      web: type === 'web' ? true : false,
      print: type === 'print' ? true : false
    }, () => {
      // calculate price if add to cart option is active
      this.calculatePrice();
    })
  }

  // calculate price for cart option
  calculatePrice = (props) => {
    let { web, print, extChecked, freezeChecked } = this.state;
    let { prices } = props || this.props;

    let price = 0;

    if (web && prices['WEB']) {
      price += prices['WEB'];
    } else if (print && prices['PRINT']) {
      price += prices['PRINT']
    }

    if (extChecked && !freezeChecked) {
      price += prices['EXTENDED_LICENSE'] ? prices['EXTENDED_LICENSE'] : 0;
    } else if (freezeChecked) {
      price += prices['MARKET_FREEZE'] ? prices['MARKET_FREEZE'] : 0;
    }

    this.setState({ price: price });
  }

  addToCart = (product) => {

    let { cart, updateCartContent, user } = this.props;
    let { web, freezeChecked, extChecked } = this.state;
    let updCart = Object.assign({}, cart);

    if (!updCart['id'] && user) {
      updCart = user.cart;
    } else if (!user && !updCart) {
      return;
    }
    if (!updCart || typeof updCart === 'undefined') return;
    let newProduct = Object.assign({}, product);
    newProduct['size'] = web ? 'WEB' : 'PRINT';

    if (freezeChecked) newProduct['isMarketFreeze'] = true;
    if (!freezeChecked) newProduct['isMarketFreeze'] = false;

    if (extChecked && !freezeChecked) newProduct['extendedLicense'] = true;
    if (!extChecked) newProduct['extendedLicense'] = false;

    if (!updCart['products']) updCart['products'] = [];
    updCart['products'].push(newProduct);

    let isGuest = user ? false : true;

    // if market freeze is enabled
    if (!isGuest && product.isMarketFreeze && user && user.user.id !== product.freezeOwnerId && !product.isAlreadyDownload) {
        return;
    } else if (isGuest && product && product.isMarketFreeze) {
      return;
    }

    updateCartContent(updCart, isGuest);
  }

  componentWillUnmount() {
    document.body.style.overflowY = 'visible';
    document.body.style.overflowX = 'hidden';
    window.removeEventListener('resize', this.handleResize);
  }


  renderDownloadBtn = (product) => {
    let { user } = this.props;
    let { extChecked, freezeChecked } = this.state;
    let userMarketFreeze = product.freezeOwnerId === (user && user.user && user.user.id);
    this.disableButton = false;

    return (
      <div className="product-add-to-cart-btn"
        onClick={product.isMarketFreeze && !userMarketFreeze &&
          (
          !product.isAlreadyDownload
          || (product.isAlreadyDownload && (extChecked || freezeChecked))
          )
          ? null : () => this.toggleDirectDownload(product.id)}>
        DOWNLOAD NOW
      </div>
    )
  }
  renderAgencyUserDownloadBtn = (product, disableBtn, trialEnded) => {
    let { user } = this.props;
    let userMarketFreeze = product.freezeOwnerId === (user && user.user && user.user.id);
    this.disableButton = !product.isAlreadyDownload && disableBtn;

    return (
      <div className="product-add-to-cart-btn"
        onClick={
          (product.isMarketFreeze && !userMarketFreeze && !product.isAlreadyDownload)
          || this.disableButton
          || trialEnded
            ? null
            : () => this.toggleDirectDownload(product.id)}>
        DOWNLOAD NOW
      </div>
    )
  }
  renderAddToCartBtn = (product) => {
    let { price, web, print } = this.state;
    let { user } = this.props;
    let userMarketFreeze = product.freezeOwnerId === (user && user.user && user.user.id);

    return (
      <div className="product-add-to-cart-btn"
        onClick={(!web && !print) || (product.isMarketFreeze && !userMarketFreeze && !product.isAlreadyDownload) ? null : () => this.addToCart(product)}>
        ADD TO CART {(web || print) && ` - $${price.toFixed(2)}`}
    </div>
    )
  }
  renderUpdateCartBtn = (product, cartIndex) => {
    let { user } = this.props;
    let { web, print } = this.state;
    let userMarketFreeze = product.freezeOwnerId === (user && user.user && user.user.id);

    return (
      <div className="product-add-to-cart-btn"
         onClick={(!web && !print) || (product.isMarketFreeze && !userMarketFreeze && !product.isAlreadyDownload) ? null : () => this.updateCartItem(product, cartIndex)}>
         UPDATE CART
       </div>
    )
  }
  renderDisabledAddToCartBtn = () => {
    let { price } = this.state;
    return (
      <div className="product-add-to-cart-btn">
        ADD TO CART - {`$${price.toFixed(2)}`}
    </div>
    )
  }
  renderCartButton = () => {

    let {
      product,
      user,
      fromCart,
      cartIndex = 0,
      cart,
      agencyUser
    } = this.props;

    let {
      extChecked,
      freezeChecked,
      web,
      print,
      printBought,
      webBought
    } = this.state;

    if (!product) return null;

    let hasPackage = user && user.user && user.user.package;
    let hasMembership = user && user.user && user.user.activeMembership;

    let hasWebCredits = false;
    let hasPrintCredits = false;

    let hasMembershipWebCredits = false;
    let hasMembershipPrintCredits = false;

    let membershipFrozen = false;

    if (hasPackage) {
      hasWebCredits = hasPackage.webSizePhotosLeft > 0;
      hasPrintCredits = hasPackage.printSizePhotosLeft > 0;
    }

    if (hasMembership) {
      hasMembershipWebCredits = hasMembership.webSizePhotosLeft > 0;
      hasMembershipPrintCredits = hasMembership.printSizePhotosLeft > 0;

      membershipFrozen = hasMembership.membershipStatus === 'FREEZE';
    }

    let userMarketFreeze = product.freezeOwnerId === (user && user.user && user.user.id);
    let productFrozen = product.isMarketFreeze;

    // Product cart status
    let inCart = false;

    if (cart && cart.products && product && !fromCart) {

      cart.products.map((el, i) => {
        if (!el) return null;
        if (el.id === product.id) {
          cartIndex = i;
          inCart = true;
        }
        return el;
      })
    }

    // Product download status
    let downloaded = product && product.isAlreadyDownload;

    // See if trial ended for agency user
    let trialEnded = agencyUser && !hasMembership;

    // if user has agency membership
    if (agencyUser) {

      let availableWeb = hasMembership && hasMembership.webSizePhotosLeft;
      let availablePrint = hasMembership && hasMembership.printSizePhotosLeft;

      // if trial period for agency user has ended

      if (trialEnded) {
        // DOWNLOAD NOW
        return this.renderAgencyUserDownloadBtn(product, true, true);
      }

      // web option chosen and web credits available
      if (web && availableWeb > 0) {
        // DOWNLOAD NOW
        return this.renderAgencyUserDownloadBtn(product, false);
      }

      // print option chosen and print credits available
      if (print && availablePrint > 0) {
        // DOWNLOAD NOW
        return this.renderAgencyUserDownloadBtn(product, false);
      }

      // print size bought and print size selected
      if (downloaded && printBought && print) {
        // DOWNLOAD NOW
        return this.renderAgencyUserDownloadBtn(product, false);
      }

      // web size bought and web size selected
      if (downloaded && webBought && web) {
        // DOWNLOAD NOW
        return this.renderAgencyUserDownloadBtn(product, false);
      }

      // DOWNLOAD NOW
      return this.renderAgencyUserDownloadBtn(product, true);
    }

    // if photo is downloaded
    if (downloaded) {

      // if user has market freeze for this product
      if (userMarketFreeze) {
        // DOWNLOAD NOW
        return this.renderDownloadBtn(product);
      }

      // if only web size is checked and web size bought
      if (web && !extChecked && !freezeChecked && webBought) {
        // DOWNLOAD NOW
        return this.renderDownloadBtn(product);
      }
      // if only print size is checked and print size bought
      if (print && !extChecked && !freezeChecked && printBought) {
        // DOWNLOAD NOW
        return this.renderDownloadBtn(product);
      }

      if (web && !extChecked && !freezeChecked && !webBought) {
        // if photo is in cart
        if (fromCart || inCart) {
          // UPDATE CART
          return this.renderUpdateCartBtn(product, cartIndex);
        }
        // ADD TO CART
        return this.renderAddToCartBtn(product);
      }
      if (print && !extChecked && !freezeChecked && !printBought) {
        // if photo is in cart
        if (fromCart || inCart) {
          // UPDATE CART
          return this.renderUpdateCartBtn(product, cartIndex);
        }
        // ADD TO CART
        return this.renderAddToCartBtn(product);
      }

      // if other options are checked
      if ((extChecked || freezeChecked) && (userMarketFreeze || !productFrozen)) {

        if (fromCart || inCart) {
          // UPDATE CART
          return this.renderUpdateCartBtn(product, cartIndex);
        }

        // ADD TO CART
        return this.renderAddToCartBtn(product);

      } else if ((extChecked || freezeChecked) && (!userMarketFreeze || productFrozen)) {

        if (web && webBought) {
          // DOWNLOAD NOW
          return this.renderDownloadBtn(product);
        }

        if (print && printBought) {
          // DOWNLOAD NOW
          return this.renderDownloadBtn(product);
        }

        return this.renderDisabledAddToCartBtn();

      }


      // DOWNLOAD NOW
      return this.renderDownloadBtn(product);
    }


    if (hasPackage || hasMembership) {

      // user has package
      if (hasPackage) {

        if (hasWebCredits && web) {

          // web type is selected && user has web credits and no additional options
          if (!extChecked && !freezeChecked) {
              // DOWNLOAD NOW
              return this.renderDownloadBtn(product);
          }
          //  web type is selected && user has web credits and additional options
          else if (extChecked || freezeChecked) {
              if (fromCart || inCart) {
                // UPDATE CART
                return this.renderUpdateCartBtn(product, cartIndex);
              }
              // ADD TO CART
              return this.renderAddToCartBtn(product);
          }

        }


        // print type is selected && user has print credits
        if (hasPrintCredits && print) {

          // print type is selected && user has print credits and no additional options
          if (!extChecked && !freezeChecked) {
            // DOWNLOAD NOW
            return this.renderDownloadBtn(product);
          }
          //  print type is selected && user hasprint credits and additional options
          else if (extChecked || freezeChecked) {
            if (fromCart || inCart) {
              // UPDATE CART
              return this.renderUpdateCartBtn(product, cartIndex);
            }
            // ADD TO CART
            return this.renderAddToCartBtn(product);
          }

        }

        // Not enough credits
        else {
          if (fromCart || inCart) {
            // UPDATE CART
            return this.renderUpdateCartBtn(product, cartIndex);
          }
          // ADD TO CART
          return this.renderAddToCartBtn(product);
        }
      }
      // user has membership
      else if (hasMembership) {

        // web type is selected && user has web credits
        if (hasMembershipWebCredits && web && !membershipFrozen) {

          // web type is selected && user has web credits and no additional options
          if (!extChecked && !freezeChecked) {
              // DOWNLOAD NOW
              return this.renderDownloadBtn(product);
          }
          //  web type is selected && user has web credits and additional options
          else if (extChecked || freezeChecked) {
            if (fromCart || inCart) {
              // UPDATE CART
              return this.renderUpdateCartBtn(product, cartIndex);
            }
              // ADD TO CART
              return this.renderAddToCartBtn(product);
          }

        }

        // print type is selected && user has print credits
        else if (hasMembershipPrintCredits && print && !membershipFrozen) {

          // print type is selected && user has print credits and no additional options
          if (!extChecked && !freezeChecked) {
            // DOWNLOAD NOW
            return this.renderDownloadBtn(product);
          }
          //  print type is selected && user hasprint credits and additional options
          else if (extChecked || freezeChecked) {
            if (fromCart || inCart) {
              // UPDATE CART
              return this.renderUpdateCartBtn(product, cartIndex);
            }
            // ADD TO CART
            return this.renderAddToCartBtn(product);
          }
        }

        // membership is frozen
        else if (membershipFrozen) {
          if (fromCart || inCart) {
            // UPDATE CART
            return this.renderUpdateCartBtn(product, cartIndex);
          }
          // ADD TO CART
          return this.renderAddToCartBtn(product);
        }

        // Not enough credits
        else {
          if (fromCart || inCart) {
            // UPDATE CART
            return this.renderUpdateCartBtn(product, cartIndex);
          }
          // ADD TO CART
          return this.renderAddToCartBtn(product);
        }

      }
    }

    // user had no package or memberhship
    else {

      if (fromCart || inCart) {
        // UPDATE CART
        return this.renderUpdateCartBtn(product, cartIndex);
      }
      // ADD TO CART
      return this.renderAddToCartBtn(product);

    }

  }

  render() {
    let {
      collections,
      w,
      width,
      h,
      extChecked,
      freezeChecked,
      web,
      print,
      initialLoading,
      isVertical,
      sliderLoading,
      loadError,
      imgHeight
    } = this.state;


    let {
      product,
      session,
      prices,
      shown,
      id,
      user,
      toggleProductVisibility,
      downloadInfoShown,
      pageMode = false,
      loggedIn,
      available,
      agencyUser
    } = this.props;

    // Membership and package options
    let hasMembership = user && user.user && user.user.activeMembership;

    // market freeze handling
    let userMarketFreeze = loggedIn && (product && product.freezeOwnerId) === (user && user.user && user.user.id);

    // trial status for agency user
    let trialEnded = agencyUser && !hasMembership;

    let productFrozen = product && product.isMarketFreeze;

    if (!pageMode && (!shown || !id)) return null;

    
    let productWrapperStyle = {
      marginBottom: 0,
      paddingBottom: 0,
      marginTop: 0
    }

	let topPanelOffset = 0;

    if (downloadInfoShown && loggedIn && (available > 0 || (agencyUser && !user.user.activeMembership)) && width > 767) {

      productWrapperStyle = {
        marginTop: 85
      }
    }

    if (window.innerWidth > 991) topPanelOffset = 140;
	  else if (window.innerWidth <= 991 && window.innerWidth > 767) topPanelOffset = 115;
	  else if (window.innerWidth <= 767) topPanelOffset = 85;


    let downloaded = product && product.isAlreadyDownload;

    let pageWrapperStyle = {
      background: pageMode ? '#fff' : 'rgba(86, 86, 86, 0.51)'
    };

    return (
      <div className="product-page-wrapper" style={pageWrapperStyle} onClick={this.closeProductFromOverlay}>
        <div className="product-scroller" style={{
          height: (h - topPanelOffset) + 15
        }}>
          {product && !initialLoading ? <div className="product-wrapper"
            style={productWrapperStyle}
          >
            <div className="product-description"
              style={{ maxWidth: pageWidthValue }}
            >
              {!pageMode && <div className="close-product" onClick={() => toggleProductVisibility(false)}></div>}
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-6 product-image-wrapper hidden-lg hidden-md">
                    <div className="grid-item full-width-grid-item">
                      <div className="product-aligner product-aligner-sm">
                        {
                          product && product.image && product.image.thumbUrl &&
                          <img
                            style={{ opacity: this.smImg ? 1 : 0 }}
                            className={loadError ? 'err' : ''}
                            onLoad={this.handleResize}
                            onError={() => this.handleResize(null, true)}
                            src={product.image.thumbUrl}
                            alt={product.caption}
                            ref={(el) => this.smImg = el}
                          />
                        }
                        <div className="gallery__imageAction">
                          <div className="round_action download-btn" onClick={() => this.toggleDirectDownload(product.id)}><i className="fa fa-long-arrow-down" aria-hidden="true"></i></div>
                          <div className="round_action" onClick={() => { this.manageFaves(product.id) }}><i className="fa fa-heart" aria-hidden="true"></i></div>
                          <div className="round_action"><i className="fa fa-plus" aria-hidden="true"></i></div>
                        </div>
                      </div>
                    </div>
                    <div className="product-share-item">
                      <div className="product-share-content">
                        <div className="product-share-widgets">
                          <div className="product-share-title">SHARE THIS IMAGE!</div>
                          <a className="pin" onClick={this.sharePin}><span className="screen-reader">share Pinterest</span></a>
                          <div className="product-share-widget">
                            <i className="fa fa-envelope-o" onClick={this.shareEmail}></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6" ref={(c) => {this.prodInfo = c}}>
                    <div className="item-title">
                    </div>
                    <div className="product-options">
                      <div className={"product-option product-options-check-round " + ((productFrozen && !userMarketFreeze && !downloaded) ? "disable-buttons" : "")}>
                        <input checked={web} disabled={productFrozen && !userMarketFreeze && !downloaded} onChange={() => this.toggleSize('web')} id="sizeWeb" type="radio" name="size" />
                        <label htmlFor="sizeWeb" className="check-wrapper">
                          <div className="check-image">
                          </div>
                        </label>
                        <div className="check-description-wrapper">
                          <div className="check-description-left">
                            <div>WEB SIZE</div>
                            <div>{isVertical ? '800x1200 | 72dpi' : '1200x800 | 72dpi'}</div>
                          </div>
                          <div className="check-description-right">
                            {
                              !agencyUser && <div>{prices['WEB'] ? `$${prices['WEB'].toFixed(2)}` : `$0.00`}</div>
                            }
                          </div>
                        </div>
                      </div>
                      <div className={"product-option product-options-check-round " + ((productFrozen && !userMarketFreeze && !downloaded) ? "disable-buttons" : "")}>
                        <input checked={print} disabled={productFrozen && !userMarketFreeze && !downloaded} onChange={() => this.toggleSize('print')} id="sizePrint" type="radio" name="size" />
                        <label htmlFor="sizePrint" className="check-wrapper">
                          <div className="check-image">
                          </div>
                        </label>
                        <div className="check-description-wrapper">
                          <div className="check-description-left">
                            <div>PRINT SIZE</div>
                            <div>{isVertical ? '3840x5760 | 300dpi' : '5760x3840 | 300dpi'}</div>
                          </div>
                          <div className="check-description-right">
                            {
                              !agencyUser && <div>{prices['PRINT'] ? `$${prices['PRINT'].toFixed(2)}` : `$0.00`}</div>
                            }
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="product-options">
                      {
                        !agencyUser &&
                        <div className="additional-product-options">ADDITIONAL OPTIONS
                        {!loggedIn && <div className="additional-options-note">Only for logged in users</div>}
                        </div>
                      }
                      {
                        !agencyUser &&
                        <div className={"product-option product-options-check-square " +
                                      (((productFrozen && !userMarketFreeze && !downloaded) || !loggedIn) ? "disable-buttons" : "")}>
                        <input checked={extChecked} disabled={((productFrozen && !userMarketFreeze && !downloaded) || !loggedIn)} onChange={this.manageExtLicense} type="checkbox" id="extLicense" />
                        <label htmlFor="extLicense" className="check-wrapper">
                          <div className="check-image">
                            <div className="check-image-check"></div>
                          </div>
                        </label>
                        <div className="check-description-wrapper">
                          <div className="check-description-left">
                            <div>EXTENDED LICENSE</div>
                            <div className="check-description-underlined">Do I need an extended license?</div>
                          </div>
                          <div className="check-description-right">
                            <div>{freezeChecked && extChecked ? `$0.00` : (loggedIn && prices['EXTENDED_LICENSE'] ? `$${prices['EXTENDED_LICENSE'].toFixed(2)}` : `$0.00`)}</div>
                          </div>
                        </div>
                      </div>
                      }
                     {
                       !agencyUser &&
                       <div className={"product-option product-options-check-square " + (((productFrozen && !userMarketFreeze && !downloaded) || !loggedIn) ? "disable-buttons" : "")}>
                        <input checked={freezeChecked} disabled={((productFrozen && !userMarketFreeze && !downloaded) || !loggedIn)} onChange={this.manageFreeze} type="checkbox" id="marketFreeze" />
                        <label htmlFor="marketFreeze" className="check-wrapper">
                          <div className="check-image">
                            <div className="check-image-check"></div>
                          </div>
                        </label>
                        <div className="check-description-wrapper">
                          <div className="check-description-left">
                            <div>JUST FOR YOU</div>
                            <div>
                              <span>6 month freeze</span>
                              <span>{` | `}</span>
                              <span className="check-description-underlined">terms and conditions</span>
                            </div>
                          </div>
                          <div className="check-description-right">
                            <div>{loggedIn && prices['MARKET_FREEZE'] ? `$${prices['MARKET_FREEZE'].toFixed(2)}` : `$0.00`}</div>
                          </div>
                        </div>
                      </div>
                    }
                    </div>
                    <div className={(
                      (productFrozen && !userMarketFreeze && !downloaded)
                      || (productFrozen && !userMarketFreeze && downloaded && (freezeChecked || extChecked))
                      || this.disableButton
                      || (!web && !print)
                      || trialEnded)
                      ? "product-add-to-cart disable"
                      : "product-add-to-cart"}
                    >
                    { this.renderCartButton() }

                      {
                        (
                          (productFrozen && !userMarketFreeze && !downloaded && !trialEnded)
                          || (productFrozen && !userMarketFreeze && downloaded && (extChecked || freezeChecked) && !trialEnded)
                        )
                        &&
                        <p className="product-market-freeze">
                          For this photo ‘Just for you’ option was activated.
                          ‘Just for you’ option will expire on {
                            new Date(product.markerFreezeExpireDate).toLocaleString("en-US", {
                              year: 'numeric',
                              month: 'long',
                              day: 'numeric',
                              timezone: 'UTC'
                            })}
                        </p>
                      }
                      {
                        userMarketFreeze && !trialEnded && <p className="product-market-freeze">You purchased ‘Just for you’ option for this photo!</p>
                      }
                      {
                        this.disableButton && !productFrozen && !userMarketFreeze && !trialEnded && <p className="product-market-freeze">You have no available credits of the selected type!</p>
                      }
                      {
                        trialEnded &&
                          <p className="product-market-freeze trial-end">Trial period for your membership has ended. <br />
                            <Link to="/checkout">Go to checkout <i className="fa fa-arrow-right"></i></Link>
                          </p>
                      }
                    </div>
                  </div>
                  <div
                    className="col-md-6 prod-image-lg product-image-wrapper hidden-sm hidden-xs"
                    style={{ height: agencyUser ? 'unset' : imgHeight, minHeight: isVertical ? 500 : '100%' }}
                  >

                    <div className="grid-item full-width-grid-item">
                    <div>
                      <div className="product-aligner" style={{ width: w }}>
                       {
                         product && product.image && product.image.thumbUrl &&
                          <img
                            style={{ opacity: this.img ? 1 : 0 }}
                            className={loadError ? 'err' : ''}
                            onLoad={this.handleResize}
                            onError={() => this.handleResize(null, true)}
                            src={product.image.thumbUrl}
                            alt={product.caption}
                            ref={(el) => this.img = el}
                          />
                        }
                          <h4 className="item-title-heading product-page-item-id-wrapper"><span className="product-page-item-id">Item: {product.id}</span></h4>
                        <div className="gallery__imageAction">
                          <div className="round_action download-btn" onClick={() => this.toggleDirectDownload(product.id)}><i className="fa fa-long-arrow-down" aria-hidden="true"></i></div>
                          <div className="round_action" onClick={() => { this.manageFaves(product.id) }}><i className="fa fa-heart" aria-hidden="true"></i></div>
                          <div className="round_action"><i className="fa fa-plus" aria-hidden="true"></i></div>
                        </div>
                      </div>

                      <div className="product-share-item">
                        <div className="product-share-content">
                          <div className="product-share-widgets">
                            <div className="product-share-title">SHARE THIS IMAGE!</div>
                            <a className="pin" onClick={this.sharePin}><span className="screen-reader">share Pinterest</span></a>
                            <div className="product-share-widget">
                              <i className="fa fa-envelope-o" onClick={this.shareEmail}></i>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                    </div>

                  </div>
                </div>
              </div>
              {
                session && session.products && session.products.length > 0 &&
                <div className="product-description-session-slider-wrapper">
                  <div className="product-description-session-title">MORE FROM THIS SESSION</div>
                  {sliderLoading && <div className="product-slider-loading">Loading...</div>}
                  <div className="product-description-session-slider">
                    <ProductSlider
                      slides={session.products}
                      pageMode={pageMode}
                      activeProduct={product}
                      imageClick={this.changeProduct}
                      type={'sessions'}
                      isVertical={isVertical}
                      sliderLoading={sliderLoading}
                    />
                  </div>
                </div>
              }
            </div>
            {
              collections && collections.length > 0 &&
              <div className="product-collections-slider-wrapper">
                <div className="product-collections-title">MORE COLLECTIONS</div>
                <div className="product-collections-slider">
                  <ProductSlider slides={collections} imageClick={() => toggleProductVisibility(null)} type={'collections'} isVertical={false} />
                </div>
              </div>
            }
          </div> : <Loader />
          }
        </div>
      </div>
    );
  };
};

export default withRouter(Product);

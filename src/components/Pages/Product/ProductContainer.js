import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Product from './Product';

import {
  selectProduct,
  selectProductId,
  toggleProductVisibility,
  getProductFromCart,
  changeProductSource,
  toggleEmailSharePopup
} from '../../../actions/product';

import {
  updateCartContent
} from '../../../actions/cart';

import {
  displayMessage
} from '../../../actions/messaging';

import {
  toggleLoginPopup
} from '../../../actions/auth';

const mapStateToProps = ({ auth, router, product, cart }) => ({
  loggedIn: auth.get('loggedIn'),
  user: auth.get('user'),
  product: product.get('product'),
  session: product.get('session'),
  prices: product.get('prices'),
  shown: product.get('shown'),
  id: product.get('id'),
  selectedPrice: product.get('selectedPrice'),
  cart: cart.get('cart'),
  fromCart: product.get('fromCart'),
  cartIndex: product.get('cartIndex'),
  cartProducts: cart.get('cartProducts'),
  emailShareShown: product.get('emailShareShown'),
  agencyUser: auth.get('agencyUser'), 
  router: router

});


const matchDispatchToProps = dispatch =>
  bindActionCreators({
    selectProduct,
    selectProductId,
    toggleProductVisibility,
    updateCartContent,
    displayMessage,
    getProductFromCart,
    changeProductSource,
    toggleEmailSharePopup,
    toggleLoginPopup
  }, dispatch);

export default connect(mapStateToProps, matchDispatchToProps)(Product);

import Home from './Pages/Home/HomeContainer';
import Product from './Pages/Product/ProductContainer';
import NotFound from './Pages/NotFound/NotFoundContainer';

export {	
	Home,
	Product,
	NotFound
};

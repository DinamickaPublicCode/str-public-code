import React, { Component } from 'react';

import './css/Sorting.css';
import CheckBox from './CheckBox';
import debounce from 'debounce';
import onClickOutside from "react-onclickoutside";
import $ from "jquery";

import filter from '../../../fake_info/filter.json'

window.$ = window.jQuery = $;

class Sorting extends Component {
	state = {
		filtersList: [],
		filtering: [],
		filtersReady: false,
		mobileSortingOpen: false,
		filterShown: true
	}

	scrollHandler = () => {
		if (window.innerWidth <= 767) {
			if (!this.state.filterShown) {
				this.setState({ filterShown: true });
				return;
			}
		}
		let scrollHeight = document.body.scrollHeight - $('footer').height();
		let scrollOffset = window.scrollY + (window.innerHeight - (window.innerHeight - $('.sorting-wrapper').height() - 275))

		if (scrollOffset >= scrollHeight && this.state.filterShown) {
			this.setState({ filterShown: false });
		}
		if (scrollOffset < scrollHeight && !this.state.filterShown) {
			this.setState({ filterShown: true });
		}
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', debounce(this.scrollHandler, 100));
		window.removeEventListener('resize', debounce(this.scrollHandler, 100));
	}
	componentDidMount() {
		window.addEventListener('scroll', debounce(this.scrollHandler, 100));
		window.addEventListener('resize', debounce(this.scrollHandler, 100));

		let { defaultFilter } = this.props;
		let filterArr = filter;
			
			let latest_index = 0
			for (let i = 0; i < filterArr.length; i++) {
				if (filterArr[i].name === defaultFilter) {
					latest_index = i;
					filterArr[i].checked = true;
				} else {
					filterArr[i].checked = false;
				}
			}
			this.setState({ filtersList: filterArr, filtersReady: true }, () => {
				this.checkBoxClick(filterArr[latest_index]['id'], filterArr[latest_index]['name'], latest_index, true)
			})
	}

	mobileSortingOpen = () => {
		if (window.innerWidth > 767) return;
		this.setState({ mobileSortingOpen: true });
	}

	handleClickOutside = () => {
		if (this.state.mobileSortingOpen) {
			this.setState({ mobileSortingOpen: false })
		}
	}

	checkBoxClick = (id, name, index, basicChange) => {
		let filtering = this.props.leftFilters,
			shouldAdd = true,
			curentVal = {
				id: id,
				name: name
			}
		if (typeof basicChange === 'undefined') {

			for (var filter in filtering) {
				if (filtering[filter].id === curentVal.id) {
					filtering.splice(filter, 1);
					shouldAdd = false;
					break;
				}
				let el = filtering[filter];
				if ((el.name === 'No People' && curentVal.name === 'People')
					|| (el.name === 'People' && curentVal.name === 'No People')
					|| (el.name === 'Horizontal' && curentVal.name === 'Vertical')
					|| (el.name === 'Vertical' && curentVal.name === 'Horizontal')) {
					filtering.splice(filter, 1);
					shouldAdd = true;
				}
			}
			if (shouldAdd) filtering.push(curentVal)

			if (this.props.onChange) this.props.onChange(filtering)

			let newList = this.state.filtersList.map((el, i) => {
				if ((el.name === 'No People' && curentVal.name === 'People')
					|| (el.name === 'People' && curentVal.name === 'No People')
					|| (el.name === 'Horizontal' && curentVal.name === 'Vertical')
					|| (el.name === 'Vertical' && curentVal.name === 'Horizontal')) {
					el.checked = false;
				}
				return el;
			});
			newList[index]['checked'] = !this.state.filtersList[index]['checked'];
			this.setState({
				filtersList: newList
			})
		} else {
			for (let filter in filtering) {
				if (filtering[filter].id === curentVal.id) {
					filtering.splice(filter, 1);
					shouldAdd = false;
					break;
				}
				let el = filtering[filter];
				if ((el.name === 'No People' && curentVal.name === 'People')
					|| (el.name === 'People' && curentVal.name === 'No People')
					|| (el.name === 'Horizontal' && curentVal.name === 'Vertical')
					|| (el.name === 'Vertical' && curentVal.name === 'Horizontal')) {
					filtering.splice(filter, 1);
					shouldAdd = true;
				}
			}
			if (shouldAdd) filtering.push(curentVal)

			if (this.props.onChange) this.props.onChange(filtering)

		}

	}
	render() {
		let { offset, isSearching } = this.props
		let { filtersReady, mobileSortingOpen, filterShown } = this.state
		let filterStyle = filtersReady && !isSearching ? { display: 'block', marginTop: offset } : { display: 'none', marginTop: offset }

		filterStyle = Object.assign({}, filterStyle, { marginLeft: filterShown ? 0 : -300 })

		return (
			<div className={"sorting-wrapper " + (mobileSortingOpen ? "opened" : "")} style={filterStyle}>
				<form action="" name="leftFilter" className="sorting__form" >
					{
						this.state.filtersList.map((element, i) => {
							return <CheckBox
												key={element.id}
												id={element.id}
												index={i}
												label={element.name}
												checked={element.checked}
												onClick={this.checkBoxClick}
											/>
						})
					}
				</form>
				<button className="mobile clickLabel" onClick={this.mobileSortingOpen}>
					<p>sort</p>
				</button>
			</div>
		);
	}
}

export default onClickOutside(Sorting);

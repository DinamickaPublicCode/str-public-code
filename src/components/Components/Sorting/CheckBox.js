import React, { Component } from 'react';

import './css/CheckBox.css';

class CheckBox extends Component {
	constructor(props) {
		super(props)
		this.state = {
			checked: props.checked
		}
	}
	componentWillReceiveProps(nextProps) {
		if (this.props.checked !== nextProps.checked) {
			this.setState({
				checked: nextProps.checked
			})
		}
	}

	handleChange(id, label, index) {
		this.props.onClick(id, label, index)
	}
	render() {
		let { id, label, index } = this.props
		let { checked } = this.state
		return (
			<div className="checkbox-group" id={label}>
				<input type="checkbox" id={id} value="true" checked={checked} name={label} onChange={() => this.handleChange(id, label, index)} />
				<label htmlFor={id}>{label}</label>
			</div>
		);
	}
}

export default CheckBox;

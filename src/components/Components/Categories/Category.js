import React, { Component } from 'react';

import './css/Category.css';

class Category extends Component {

	render() {
		let { id, coverImg, title, onClick } = this.props
		return (
			<div className="category col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-xs" key={id}>
				<div className="category__innerWrapper">
					<div className='category__linkWrapper' onClick={onClick}>
						<div className="category__featuredImg" style={{ backgroundImage: `url(${coverImg})` }} >
							<div className="category__title">{title}</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Category;

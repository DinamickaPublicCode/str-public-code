import React, { Component } from 'react';
import Category from './Category';
import { withRouter } from 'react-router-dom';
import './css/Categories.css';

class Categories extends Component {
  constructor() {
    super();
    this.state = {
      categoryList: [],
      cls: 'categories-closed',
      overlayDisplay: 'none',
      activeFilter: 0
    }

    this.handleViewAll = this.handleViewAll.bind(this)
    this.handleViewCategory = this.handleViewCategory.bind(this)
    this.toggleOpen = this.toggleOpen.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    this.toggleOpen(nextProps)
  }

  toggleOpen(nextProps) {
    if (nextProps.open && this.state.cls !== 'categories-open') {
      this.setState({
        cls: 'categories-open'
      })

    } else if (!nextProps.open && this.state.cls !== 'categories-closed') {
      this.setState({
        cls: 'categories-closed'
      })
    }
  }

  switchFilter(next) {
    this.setState({
      activeFilter: next
    })
  }

  handleViewAll() {
    this.props.close();
    this.props.history.push('/categories');
  }

  handleViewCategory(id) {
    this.props.close();
    this.props.history.push('/categories/' + id);
  }

  render() {
    let { cls } = this.state
    let { categories = [] } = this.props
    let { PUBLIC_URL } = process.env

    return (
      <div className={"categories-wrapper " + cls} >
        <div className="categories__innerWrapper">
          <div className="container-fluid">
            <div className="row">
              {
                categories.map((categ) => {
                  return <Category
                    onClick={() => this.handleViewCategory(categ.id)}
                    key={categ.id}
                    href={'/categories/' + categ.id}
                    coverImg={categ.image ? categ.image.thumbUrl : (PUBLIC_URL + '/images/slide-placeholder.png')}
                    title={categ.name}
                  />
                })
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Categories);

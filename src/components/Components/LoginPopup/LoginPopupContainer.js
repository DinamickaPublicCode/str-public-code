import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LoginPopup from './LoginPopup';

import {
  toggleLoginPopup,
  toggleSignupPopup
} from '../../../actions/auth';

import {
  displayMessage
} from '../../../actions/messaging';

import {
  setScrollPosition
} from '../../../actions/scroll';


const mapStateToProps = ({ auth, router, product }) => ({
  loggedIn: auth.get('loggedIn'),
  user: auth.get('user'),
  shown: auth.get('loginPopupShown'),
  authMessage: auth.get('authMessage'),
  productPageOpen: product.get('shown'),
  router: router
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({
    displayMessage,
    toggleLoginPopup,
    toggleSignupPopup,
    setScrollPosition
  }, dispatch);

  export default connect(mapStateToProps, matchDispatchToProps)(LoginPopup);

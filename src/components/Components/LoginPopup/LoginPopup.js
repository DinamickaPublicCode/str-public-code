import React, { Component } from 'react';
import './css/LoginPopup.css';
import './css/Login.css';
import { AuthGroup } from '../';
import { Link, withRouter } from 'react-router-dom';

let emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class LoginPopup extends Component {
  state = {
    email: '',
    password: ''
  }

  togglePopup = (e) => {
    if (!e.target.className.includes('login-popup-scroller')) return;
    let { toggleLoginPopup } = this.props;
    toggleLoginPopup(false);
  }

  componentDidMount() {
    document.onkeydown = (e) => {
      if ((e.keycode === 13 || e.which === 13) && this.props.shown) {
        this.loginWithData(this.state);
      }
    }
  }

  handleChange = (key, val) => {

    let newState = Object.assign({}, this.state);
    newState[key] = val;

    this.setState(newState);
  }

  loginForm = () => {
    return Object.keys(this.state).map(key => {
        return (
            <div key={key} className="form-group">
                <input
                    className="login-input"
                    value={this.state[key]}
                    type={key === 'password' ? 'password' : 'text'}
                    placeholder={key.toUpperCase()}
                    onChange={(e) => this.handleChange(key, e.target.value)}
                />
            </div>
        )
    })
  }

  handleSubmit = () => {
      this.loginWithData(this.state);
  }

  loginWithData = (data) => {
    let { displayMessage } = this.props;
      if (data.email === "") {
        displayMessage({type: 'error', text:  'Email required!'})
        return;
      }
      if (!emailRegex.test(data.email)) {
        displayMessage({type: 'error', text:  'Invalid email!'})
        return;
      }
      if (data.password === "") {
        displayMessage({type: 'error', text:  'Password required!'})
        return;
      }

      this.clearLoginData();
  }

  clearLoginData = () => {
      this.setState({
        email: '',
        password: ''
      })
  }

  switchToSignup = () => {
    let {
      toggleSignupPopup,
      toggleLoginPopup
    } = this.props;

    toggleLoginPopup(false);
    toggleSignupPopup(true);
  }


  render() {
    let { shown } = this.props;

    if (!shown) return null;
    return (
      <div className="login-popup-wrapper">
        <div className="login-popup-scroller" onClick={this.togglePopup}>
          <div className="login-popup-body">
            <div className="login-wrapper__page-aligner">
                  <div className="login-container">
                      <div className="login-container__form-wrapper">
                          <div className="login-form">
                              <h2 className="login-title"><span>LOGIN</span></h2>
                              <form name="login">
                                  {this.loginForm()}
                              </form>
                              <div className="auth-button-group">
                                  <AuthGroup
                                      formKey={'login'}
                                      hasCheckbox={false}
                                      onClick={() => this.handleSubmit('login')}
                                  />
                              </div>
                              <div className="forgot-password-link">
                                  <a className="create-acc" onClick={() => {this.switchToSignup()}}>
                                    <span>{`Don't have an account? Create one!`}</span>
                                  </a>
                                  <Link to="/forgot-password">
                                      <span>Forgot your password?</span>
                                  </Link>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
         </div>
      </div>
    )
  }
}

export default withRouter(LoginPopup);

import React, { Component } from 'react';
import Masonry from 'react-masonry-component';
import './css/Gallery.css';
import InfiniteScroll from 'react-infinite-scroller';
import Loader from './Loader';
import { ImagePreloader } from '../';

let masonryOptions = {
  transitionDuration: 900,
  itemSelector: '.grid-item',
  columnWidth: '.grid-sizer',
  gutter: '.gutter-sizer',
  percentPosition: true,
};

class ImageTemplate extends Component {
  render() {
    let { clickable,
      addToFaves,
      removeFromFaves,
      type,
      alttxt,
      el = {},
      src,
      showAlbumPopup,
      toggleDownloadPopup,
      index,
      isFave
    } = this.props

    return (
      <div
        style={{ cursor: clickable ? 'pointer' : 'default' }}
        className="grid-item"
        onClick={(e) => {
          if (!e.target.className.includes('fa')
            && !e.target.className.includes('round_action')) {
            this.props.imageClick(el.id)
          }
        }}>
        <ImagePreloader src={src} alttxt={alttxt} />
        <div className="gallery__imageAction">
          {!el.markerFreeze ? <div className="round_action ss download-btn" onClick={() => toggleDownloadPopup(true, el)}><i className="fa fa-long-arrow-down" aria-hidden="true"></i></div> : null}
          {isFave ? null : <div className="round_action " onClick={() => { addToFaves(type, el, index) }}><i className="fa fa-heart" aria-hidden="true"></i></div>}
          {isFave ? <div className="round_action isFav" onClick={() => { removeFromFaves(type, el, index) }}><i className="fa fa-heart" aria-hidden="true"></i></div> : null}
          <div className="round_action" onClick={() => showAlbumPopup(el)}><i className="fa fa-plus" aria-hidden="true"></i></div>
        </div>
      </div>
    )
  }
}

class Gallery extends Component {
  state = {
    respond: {},
    activeProduct: null,
    showAlbumPopup: false
  }

  getLoader = () => {
    return (
      <Loader key="loader"/>
    )
  }

  addToFaves = (type, val, index) => {
    let requestdata = {}
    requestdata[type] = val

    if (!this.props.loggedIn
      && this.props.toggleLoginPopup
      && typeof this.props.toggleLoginPopup === 'function') {
      this.props.toggleLoginPopup();
      return;
    } else if (!this.isLoggedIn) return;


    console.log('Add to favourites', type, requestdata);
    let {
      images,
      updateGallery,
      updateCart,
      cart,
      updateFavorites
    } = this.props;

    if (updateCart) updateCart(cart, val.id, true);
    updateFavorites(type, val, true);

    images[index]['inFavorites'] = true;
    updateGallery(images);

  }

  removeFromFaves = (type, val, index) => {
    let requestdata = {}
    requestdata[type] = val
    console.log('Remove from favourites', type, requestdata);
    let {
      images,
      updateGallery,
      updateCart,
      cart,
      updateFavorites
    } = this.props;

    if (updateCart) updateCart(cart, val.id, false);
    images[index]['inFavorites'] = false;
    updateGallery(images);
    updateFavorites(type, val, false);

  }


  render() {

    let {
      clickable,
      images,
      preloader,
      OnLoadMore,
      hasMore,
      imageClick,
      showAmount,
      type = 'product',
      addToAlbum,
      toggleDownloadPopup,
      updateCart,
      modifiedFaves = {},
      loggedIn
    } = this.props;

    let Template = this.props.imageTemplate || ImageTemplate;
    let { PUBLIC_URL } = process.env;

    let ChildImages = images.map((element, i) => {

      let isFave = element.inFavorites;

      if (modifiedFaves[type] && modifiedFaves[type][element.id] === 1) {
        isFave = true;
      } else if (modifiedFaves[type] && modifiedFaves[type][element.id] === 0) {
        isFave = false;
      }

      if (!loggedIn) isFave = false;

      return <Template
        type={type}
        index={i}
        isFave={isFave}
        addToFaves={this.addToFaves}
        removeFromFaves={this.removeFromFaves}
        newFaves={this.state.newFaves}
        el={element}
        totalProducts={element.totalProducts || 0}
        showAmount={showAmount}
        key={element.image.id}
        id={element.id}
        src={element.src || element.image.thumbUrl || PUBLIC_URL + '/images/slide-placeholder.png'}
        alttxt={element.altText || element.alttxt}
        name={element.name || ''}
        imageClick={imageClick}
        toggleDownloadPopup={toggleDownloadPopup}
        showAlbumPopup={addToAlbum}
        clickable={clickable || true}
        updateCart={updateCart}
      />
    })

    return (
      <div className={preloader ? 'Gallery-wrapper not-ready' : 'Gallery-wrapper'}>
        {(preloader !== true && images.length === 0) ? <div className="no-data-placeholder">No data</div> :
          (!hasMore && images.length === 0) ? <div className="no-data-placeholder">No data</div> : null}

        <InfiniteScroll
          element='div'
          className="infinite-wrapper"
          pageStart={0}
          loadMore={OnLoadMore}
          hasMore={hasMore}
          threshold={0}
          loader={this.getLoader()}>
          <Masonry
            className={preloader ? 'grid not-ready' : 'grid'}
            elementType={'div'}
            options={masonryOptions}
            disableImagesLoaded={false}
            updateOnEachImageLoad={true}
          >
            <div className="grid-sizer"></div>
            <div className="gutter-sizer"></div>
            {ChildImages}
          </Masonry>
        </InfiniteScroll>
      </div>
    );
  }
}

export default Gallery;

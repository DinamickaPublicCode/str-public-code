import React, { Component } from 'react';
import './css/ToggleView.css';

class ToggleView extends Component {

  render() {
    let {filter, onClick} = this.props 
    return (
      <div className="ToggleView-wrapper">
        <div className="container-fluid">
            <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12">
                    <div className="toogleview">
                        <div className={"toogleview__item " + (filter === 'THIS_MONTH' ? 'selected' : '')} onClick={() => onClick('THIS_MONTH')}>this  month</div>
                        <div className={"toogleview__item " + (filter === 'ALL' ? 'selected' : '')} onClick={() => onClick('ALL')}>all</div>
                    </div>
                </div>
            </div>

        </div>
      </div>
    );
  }
}

export default ToggleView;

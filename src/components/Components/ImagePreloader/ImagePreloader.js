import React, { Component } from 'react';
import './css/ImagePreloader.css';

export default class ImagePreloader extends Component {
  state = {
    loading: true,
    loadFailed: false
  }
  render() {
    let { src, alttxt, onClick, className="" } = this.props;
    let { loading, loadFailed } = this.state;
    return (
      <div className="image-preloader">
      { loading &&
        <div className="image-preloader-content">
          <div className="image-preloader-text">Loading...</div>
        </div>
      }
      {
        loadFailed && !loading &&
        <div className="image-preloader-content">
          <div className="image-preloader-text">Failed to load</div>
        </div>
      }
      {
        !loadFailed &&
          <img
          src={src}
          alt={alttxt}
          className={className}
          onClick={onClick || null}
          onLoad={() => this.setState({ loading: false })}
          onError={() => this.setState({ loading: false, loadFailed: true })}
        />
      }
      </div>
    )
  }
}

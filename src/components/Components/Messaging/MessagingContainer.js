import { connect } from 'react-redux';
import Messaging from './Messaging';

const mapStateToProps = ({ messaging }) => ({
  message: messaging.get('message')
});

const matchDispatchToProps = null;

export default connect(mapStateToProps, matchDispatchToProps)(Messaging);

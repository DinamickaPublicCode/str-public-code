import React, {Component} from 'react'
import './css/Messaging.css'

export default class Messaging extends Component {
  state = {
    messageCls: ""
  }
  show = (type) => {
      if (!type) type = this.props.type || '';

      this.setState({
          messageCls: type === 'error' ? 'active error' : 'active'
      });

      let _this = this;
      setTimeout(() => {
         _this.setState({
             messageCls: ""
         })
      }, 4500)
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.message !== nextProps.message && nextProps.message) {
      this.show(nextProps.message.type);
    }
  }

  render() {
    let { messageCls } = this.state
    let { text, message = {} } = this.props
    return (
      <div className={"message " + messageCls}>{text || message.text}</div>
    )
  }
}

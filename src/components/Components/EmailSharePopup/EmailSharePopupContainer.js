import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import EmailSharePopup from './EmailSharePopup';

import {
  toggleEmailSharePopup
} from '../../../actions/product';

import {
  displayMessage
} from '../../../actions/messaging';

import {
  setScrollPosition
} from '../../../actions/scroll';

const mapStateToProps = ({ product }) => ({
  shown: product.get('emailShareShown'),
  product: product.get('product'),
  productPageOpen: product.get('shown') 
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({
    toggleEmailSharePopup,
    displayMessage,
    setScrollPosition
  }, dispatch);

export default connect(mapStateToProps, matchDispatchToProps)(EmailSharePopup);

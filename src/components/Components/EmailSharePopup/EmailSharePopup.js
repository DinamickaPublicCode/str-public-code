import React, { Component } from 'react';
import './css/EmailSharePopup.css';

export default class EmailSharePopup extends Component {
  closePopup = (e) => {
    if (e.target.className.includes('email-popup-wrapper') ||
      e.target.className.includes('email-popup-cancel')) {
      this.props.toggleEmailSharePopup(false);
    }
  }
  copyLink = () => {
    let { displayMessage, toggleEmailSharePopup } = this.props;
    let ref = this.refs.share;
    ref.select();
    let cmd = document.execCommand("Copy");
    if (cmd) {
      displayMessage({type: 'success', text: 'Copied link to clipboard'});
      toggleEmailSharePopup(false);
    } else {
      this.iosCopyToClipboard(ref);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.shown !== nextProps.shown) {
      if (nextProps.shown && !nextProps.productPageOpen) {
        this.scrollOffset = window.scrollY;
        this.props.setScrollPosition(window.scrollY);
        window.$('body').css({"overflow": "hidden", "position": "fixed", "marginTop": -window.scrollY});
        window.$('#root').css({"overflow": "hidden", "position": "fixed", "marginTop": -window.scrollY});
      } else {
        window.$('body').css({"overflow": "visible", "position": "static", "marginTop": 0});
        window.$('#root').css({"overflow": "visible", "position": "static", "marginTop": 0});
        this.props.setScrollPosition(0);
        window.scrollTo(0, this.scrollOffset || 0);
      }
    }
  }

  iosCopyToClipboard = (el) => {
    let { displayMessage, toggleEmailSharePopup } = this.props;
    let oldContentEditable = el.contentEditable,
        oldReadOnly = el.readOnly,
        range = document.createRange();

    el.contenteditable = true;
    el.readonly = false;
    range.selectNodeContents(el);

    var s = window.getSelection();
    s.removeAllRanges();
    s.addRange(range);

    el.setSelectionRange(0, 999999);

    el.contentEditable = oldContentEditable;
    el.readOnly = oldReadOnly;

    let cmd = document.execCommand('copy');
    if (cmd) {
      displayMessage({type: 'success', text: 'Copied link to clipboard'});
      toggleEmailSharePopup(false);
    } else {
      displayMessage({type: 'error', text: 'Failed to copy link'});
    }
}
  render() {
    let { shown, product = {} } = this.props;
    if (!shown) return null;

    let path = window.location.protocol + '//'
      + window.location.hostname
      + (window.location.port ? ':' + window.location.port : '') + '/';

    return (
      <div className="email-popup-wrapper" onClick={this.closePopup}>
        <div className="email-popup-body">
          <div>
            <div className="email-popup-title">
              Share
            </div>
            <div className="email-popup-input">
              <input value={path + 'product/' +  product.id} ref="share" readOnly/>
            </div>
            <div className="email-popup-buttons">
              <div className="email-popup-cancel" onClick={this.closePopup}>CANCEL</div>
              <div className="email-popup-copy" onClick={this.copyLink}>COPY LINK</div>
            </div>
            </div>
        </div>
      </div>
    );
  }
}

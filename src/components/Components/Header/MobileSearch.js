import React, { Component } from 'react';
import onClickOutside from "react-onclickoutside";
import Select from 'react-select-plus';

class MobileSearch extends Component {
  handleClickOutside = (e) => {
    if (window.$(e.target).hasClass('fa fa-search') || window.$(e.target).hasClass('action__search') ) {
        return
    }
      this.props.closeSearch();
  };
  render() {
    let { searchParam,
          searchOptions,
          handleSearchParamChange,
          isOpenSearch,
          suggestions,
          suggestInputChange,
          searchSubject,
          suggestAjaxInProgres,
          suggestionClick,
          startSearch } = this.props;
    return (
      <div className={`search__mobile  ${isOpenSearch && "search__mobile-open"}`} >
        <div className="search">
            <form className="search-form" onSubmit={startSearch}>
                <div className={suggestions.length ? 'search__formField is-suggested' : 'search__formField'}>
                  <input
                    type="text"
                    required
                    value={searchSubject}
                    onChange={suggestInputChange}
                    autoComplete="off"
                    placeholder="Search photos"
                    className="search__field-input"
                    id="search_input_mobile"
                  />
              <div className="search_suggestion-container">
                {
                  suggestions.map((element, index) => {
                    return (
                      <div className="search_suggestion-item" key={index} onClick={() => { suggestionClick(element) }}>{element}</div>
                    )
                  })
                }
              </div>
              <div className={suggestAjaxInProgres ? 'suggest_ajax visble' : 'suggest_ajax invisble'}></div>
              <div className="search-categories">
                <Select
                  placeholder={'ALL'}
                  labelKey={'name'}
                  valueKey={'value'}
                  value={searchParam}
                  options={searchOptions}
                  onChange={handleSearchParamChange}
                />
              </div>
              <button className="search__submit-btn"><i className="fa fa-search" aria-hidden="true"></i></button>
            </div>
            </form>
        </div>
      </div>
    );
  };
};

export default onClickOutside(MobileSearch);

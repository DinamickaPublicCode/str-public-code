import React, { Component } from 'react';
import onClickOutside from "react-onclickoutside";
import { Link } from 'react-router-dom';

class MobileMenu extends Component {
  handleClickOutside = (e) => {
    if (!window.$(e.target).hasClass('action__openMenu')
      && !window.$(e.target).parent().hasClass('action__openMenu')
      && !window.$(e.target).parent().parent().hasClass('action__openMenu')) {
        this.props.toggleMobileMenu(false);
      }
  };
  render() {
    let { logOut, loggedIn, hidePricing } = this.props;
    return (
      <div className="row navigation" id="mobileMenu">
        <div className="col-xs-12">
        {!loggedIn ?
        <ul className="navigation__list">
          <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/" className="navigation__link active">featured</Link></li>
          <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/categories" className="navigation__link">categories</Link></li>
          <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/collections" className="navigation__link">collections</Link></li>
          <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/sessions" className="navigation__link">sessions</Link></li>
        </ul>
          : <ul className="navigation__list">
              <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/" className="navigation__link active">featured</Link></li>
              <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/categories" className="navigation__link">categories</Link></li>
              <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/collections" className="navigation__link">collections</Link></li>
              <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/sessions" className="navigation__link">sessions</Link></li>
              <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/dashboard" className="navigation__link">dashboard</Link></li>
              {
                !hidePricing &&
                <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/pricing" className="navigation__link">pricing</Link></li>
              }
              {
                hidePricing &&
                <li className="navigation__listItem" onClick={() => this.props.toggleMobileMenu(false)}><Link to="/no-pricing" className="navigation__link">pricing</Link></li>
              }
              <li className="navigation__listItem" onClick={() => logOut()}><a className="navigation__link">log out</a></li>
            </ul> }
        </div>
      </div>
    );
  };
};

export default onClickOutside(MobileMenu);

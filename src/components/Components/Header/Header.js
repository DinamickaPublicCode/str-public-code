import React, { Component } from 'react';
import './css/Header.css';
import { Link } from 'react-router-dom'
import $ from "jquery";
import API from '../../../api';
import { withRouter } from 'react-router-dom';
import debounce from 'debounce';
import Select from 'react-select-plus';
import 'react-select-plus/dist/react-select-plus.css';

import {
  CollectionsSlider,
  Categories,
} from '../'

import MobileMenu from './MobileMenu';
import MobileSearch from './MobileSearch';

window.$ = window.jQuery = $;

const animationDelay = 600;
let oldSearchValue = "";

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sessions: [],
      collections: [],
      categories: [],
      collectionsShown: false,
      sessionsShown: false,
      categoriesShown: false,
      userDropdownShown: false,
      collectionsClosing: false,
      sessionsClosing: false,
      activeTab: null,
      userData: {},
      isAnimating: false,
      loggedIn: props.loggedIn || undefined,
      w: 0,
      mobileMenuOpen: false,
      suggestions: [],
      SearchValue: '',
      SScurrentWord: "",
      SScurrentWordIndex: 0,

      searchParam: null,

      categoryOptions: [],
      sessionOptions: [],
      collectionOptions: [],
      isOpenSearch: false
    }

    this.handleResize = this.handleResize.bind(this)
    this.toggleMobileMenu = (shouldOpen, ignoreTimeout) => {

      if (this.isAnimating && !ignoreTimeout) return;
      this.isAnimating = true;

      setTimeout(() => { this.isAnimating = false }, 200);
      if (shouldOpen) {
        this.setState({ mobileMenuOpen: true }, () => {
          $('#mobileMenu').slideDown(200);
        })
      } else {
        this.setState({ mobileMenuOpen: false }, () => {
          $('#mobileMenu').slideUp(200);
        })
      }

    }

    this.toggleSearch = () => {
      const { isOpenSearch } = this.state;
      this.setState({isOpenSearch: !isOpenSearch});
    }
    this.closeSearch = () => {
      this.setState({isOpenSearch: false});
    }

    this.endAnimation = (delay) => {
      setTimeout(() => {
        this.setState({ isAnimating: false })
      }, delay)
    }

    this.getSessions = () => {
      API.getSessions()
        .then((sessions => {

          let sessionOptions = sessions.map((el, i) => {
            return {
              name: el.userSessionName,
              value: el
            }
          })

          this.setState({
            sessions: sessions,
            sessionOptions: sessionOptions
          })
        }))
    }

    this.getCollections = () => {
      API.getCollections()
        .then((collections => {

          let collectionOptions = collections.map((el, i) => {
            return {
              name: el.name,
              value: el
            }
          })

          this.setState({
            collections: collections,
            collectionOptions: collectionOptions
          })
        }))
    }

    this.getCategories = () => {
      API.getCategories()
        .then((categories) => {
          let categoryOptions = categories.map((el, i) => {
            return {
              name: el.name,
              value: el
            }
          })

          this.setState({
            categories: categories,
            categoryOptions: categoryOptions
          })
        })
    }

    this.clickCollections = () => {
      if (this.state.isAnimating) return;
      if (!this.state.collectionsShown) this.getCollections();
      if (this.state.sessionsShown || this.state.categoriesShown) {
        this.setState({
          sessionsShown: false,
          categoriesShown: false,
          activeTab: 2,
          isAnimating: true
        }, () => {
          let _this = this;
          setTimeout(() => {
            _this.setState({
              collectionsShown: !_this.state.collectionsShown,
            }, () => _this.endAnimation(0))
          }, animationDelay)
        })
      } else {
        this.setState({
          collectionsShown: !this.state.collectionsShown,
          sessionsShown: false,
          categoriesShown: false,
          activeTab: 2,
          isAnimating: true
        }, () => this.endAnimation(animationDelay))
      }

    }

    this.clickSessions = () => {
      if (this.state.isAnimating) return;
      if (!this.state.sessionsShown) this.getSessions();
      if (this.state.collectionsShown || this.state.categoriesShown) {
        this.setState({
          collectionsShown: false,
          categoriesShown: false,
          activeTab: 3,
          isAnimating: true
        }, () => {
          let _this = this;
          setTimeout(() => {
            _this.setState({
              sessionsShown: !_this.state.sessionsShown
            }, () => _this.endAnimation(0))
          }, animationDelay)
        })
      } else {
        this.setState({
          collectionsShown: false,
          categoriesShown: false,
          sessionsShown: !this.state.sessionsShown,
          activeTab: 3,
          isAnimating: true
        }, () => {
          this.endAnimation(animationDelay)
        })
      }
    }

    this.clickFeatured = () => {
      this.setState({
        activeTab: 0,
        collectionsShown: false,
        sessionsShown: false,
        categoriesShown: false
      }, () => {
        this.props.toggleProductVisibility(null);
        this.endAnimation()
        this.props.history.push('/')

      })
    }

    this.clickCategories = () => {
      if (this.state.isAnimating) return;
      if (!this.state.categoriesShown) this.getCategories();
      if (this.state.collectionsShown || this.state.sessionsShown) {
        this.setState({
          activeTab: 1,
          collectionsShown: false,
          sessionsShown: false,
          isAnimating: true
        }, () => {
          let _this = this;
          setTimeout(() => {
            _this.setState({
              categoriesShown: !_this.state.categoriesShown,

            }, () => _this.endAnimation(0))
          }, animationDelay)
        })
      } else {
        this.setState({
          activeTab: 1,
          collectionsShown: false,
          sessionsShown: false,
          categoriesShown: !this.state.categoriesShown,
          isAnimating: true
        }, () => this.endAnimation(animationDelay))
      }
    }

    this.handleSlideClick = (id, name) => {
      let { toggleProductVisibility } = this.props;
      toggleProductVisibility(false);
      if (this.state.collectionsShown) {
        this.clickCollections();
      }
      if (this.state.sessionsShown) {
        this.clickSessions();
      }
      this.props.history.push('/' + name + '/' + id)
    }

    this.closeCategoriesTab = () => {
      this.setState({
        categoriesShown: false,
        isAnimating: false
      })
    }
    this.closeCollectionsTab = () => {
      this.setState({
        collectionsShown: false,
        collectionsClosing: true,
        isAnimating: true
      }, () => {
        let _this = this
        setTimeout(() => {
          _this.setState({
            collectionsClosing: false,
            isAnimating: false
          })
        }, animationDelay)
      })
    }
    this.closeSessionsTab = () => {
      this.setState({
        sessionsShown: false,
        sessionsClosing: true,
        isAnimating: true
      }, () => {
        let _this = this
        setTimeout(() => {
          _this.setState({
            sessionsClosing: false,
            isAnimating: false
          })
        }, animationDelay)
      })
    }
    
    this.toggleDownloadInfo = () => {
      let { toggleDownloadInfo, downloadInfoShown } = this.props;
      toggleDownloadInfo(!downloadInfoShown);
    }
  }

  componentDidMount() {
    this.getSessions();
    this.getCollections();
    this.getCategories();
    this.handleResize();
    window.onresize = debounce(this.handleResize, 100);
    document.onclick = this.documentClick;
    // INIT STRIPE
    window.Stripe.setPublishableKey('pk_test_7QTGDAaO3FEyfZlfy4kNDe1X');
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loggedIn !== nextProps.loggedIn && !nextProps.loggedIn) {
      this.props.history.push('/');
    }
    if (this.props.location.pathname !== nextProps.location.pathname) {
      this.toggleMobileMenu(false, true);
      if (this.state.collectionsShown) {
        this.clickCollections();
      }
      if (this.state.sessionsShown) {
        this.clickSessions();
      }
      if (this.state.categoriesShown) {
        this.clickCategories();
      }
    }
  }

  closeAllSlides = () => {
    if (this.state.isAnimating) return;
    this.setState({
      activeTab: 1,
      collectionsShown: false,
      sessionsShown: false,
      categoriesShown: false,
      isAnimating: true
    }, () => this.endAnimation(animationDelay))
  }

  componentWillUnmount() {
    window.onresize = null;
  }

  handleResize = () => {
    this.setState({ w: window.innerWidth })
  }

  manageProductSelection = () => {
    let { toggleProductVisibility } = this.props;
    toggleProductVisibility(false);
  }

  logOut = () => {
    this.props.logOut();
  }

  suggestionClick = (value) => {
    let buffer = this.props.searchSubject.trim().split(' ');
    buffer[this.state.SScurrentWordIndex] = value;
    buffer = buffer.join(' ');
    buffer += ' ';
    this.props.setSearchSubject(buffer);
    this.setState({suggestions:[]});
    document.getElementById('search_input').focus();
  }

  AnylazeSearchstring = (value) => {
    let strArrayNEW = value.trim().split(' ');
    let strArrayOLD = oldSearchValue.trim().split(' ');

    if (strArrayNEW.leght === 1) {

        this.setState({SScurrentWordIndex :0})
    } else {
      for (let index = 0; index < strArrayNEW.length; index++) {
        if (strArrayNEW[index] !== strArrayOLD[index]) {
            this.setState({SScurrentWordIndex :index})
        }
      }
    }
    oldSearchValue = value;
    
  }

  suggestInputChange = (e) => {
    let value = e.target.value;
    this.props.setSearchSubject(e.target.value);
    this.AnylazeSearchstring(value);
  }

  startSearch = (e) => {
    e.preventDefault();
    this.setState({suggestions:[]});
    this.props.history.push('/search')
  }

  documentClick = (e) => {
    if(e.target.className !== "search_suggestion-item" && e.target.id !== "search_input" ){
      this.setState({suggestions:[]});
    }
  }

 
  getSearchOptions = () => {
    let { sessionOptions, collectionOptions } = this.state;

    return [
      { name: 'Collections', options: collectionOptions },
      { name: 'Sessions', options: sessionOptions }
    ];
  }

  render() {

    let {
      collectionsShown,
      sessionsShown,
      sessions,
      categories,
      categoriesShown,
      collections,
      activeTab,
      collectionsClosing,
      sessionsClosing,
      w,
      mobileMenuOpen,
      isAnimating,
      searchParam,
      isOpenSearch
    } = this.state

    let {
      loggedIn,
      user,
      toggleCart,
      downloadInfoShown,
      available,
      agencyUser
    } = this.props;

    if (!loggedIn) downloadInfoShown = false;

    let searchOptions = this.getSearchOptions();

    let sliderHeight = 600
    let categoriesHeight = 800
    let { noActiveTab, cartLength } = this.props
    let path = this.props.history.location.pathname

    let _activeTab = activeTab || 0;

    if (noActiveTab) {
      _activeTab = -1
    }
    if (collectionsShown || collectionsClosing) _activeTab = 2
    else if (sessionsShown || sessionsClosing) _activeTab = 3
    else if (categoriesShown) _activeTab = 1
    else if (path.includes('/collections')) {
      _activeTab = 2
    } else if (path.includes('/categories')) {
      _activeTab = 1
    } else if (path.includes('/sessions')) {
      _activeTab = 3
    } else if (path === '/') _activeTab = 0
    else _activeTab = -1

    if (isAnimating && _activeTab === 0) _activeTab = null;

    let loginActive = path.includes('/login');
    let pricingActive = path.includes('/pricing');

    return (
      <div className={downloadInfoShown && ((available > 0 || agencyUser) && !user.user.activeMembership) &&  w > 767 ? "downloads-open" : ""}>
        <div className="header-wrapper"></div>
        <header className="header-wrapper fixed"
          id="top">
          <div className="download-info-aligner" style={downloadInfoShown && (available > 0 || agencyUser) ? { overflow: 'auto', height: 70 } : { overflow: 'hidden', height: 0 }}>
           
          </div>
          <div className="header-innerWrapper">
            <div className="container-fluid desktop">
              <div className="header-aligner">
                <Link to="/" onClick={() => { this.props.toggleProductVisibility(false); this.closeAllSlides() }} className="header-logo-section logo">
                  {path.includes('dashboard') ? <div className="logo__container-dashboard-image"></div> : <div className="logo__container-bgimage"></div>}
                </Link>
                <div className="header-controls-section">
                  <div className="row search">
                    <div className="col-sm-12">
                    <form className="search-form" onSubmit={this.startSearch}>
                      <div className={this.state.suggestions.length ? 'search__formField is-suggested' : 'search__formField'}>
                        <input
                          type="text"
                          required
                          value={this.props.searchSubject}
                          onChange={this.suggestInputChange}
                          autoComplete="off"
                          placeholder="Search photos"
                          className="search__field-input"
                          id="search_input"
                        />
                        <div className="search_suggestion-container">
                          {
                            this.state.suggestions.map((element, index) => {
                              return (
                                <div className="search_suggestion-item" key={index} onClick={() => { this.suggestionClick(element) }}>{element}</div>
                              )
                            })
                          }
                        </div>
                        <div className={this.state.suggestAjaxInProgres ? 'suggest_ajax visble' : 'suggest_ajax invisble'}></div>
                        <div className="search-categories">
                          <Select
                            placeholder={'ALL'}
                            labelKey={'name'}
                            valueKey={'value'}
                            value={searchParam}
                            options={searchOptions}
                            onChange={null}
                          />
                        </div>
                        <button className="search__submit-btn"><i className="fa fa-search" aria-hidden="true"></i></button>
                      </div>
                      </form>
                    </div>
                  </div>
                  <div className="row navigation">
                    <div className="col-sm-12">
                      <ul className="navigation__list">
                        <li className="navigation__listItem"><div
                          className={"navigation__link " + (_activeTab === 0 ? "active" : "")}
                          onClick={() => this.clickFeatured()}>featured</div></li>
                        <li className="navigation__listItem"><div
                          className={"navigation__link " + (_activeTab === 1 ? "active" : "")}
                          onClick={() => this.clickCategories()}>categories</div></li>
                        <li className="navigation__listItem"><div
                          className={"navigation__link " + (_activeTab === 2 ? "active" : "")}
                          onClick={() => this.clickCollections()}>collections</div></li>
                        <li className="navigation__listItem"><div
                          className={"navigation__link " + (_activeTab === 3 ? "active" : "")}
                          onClick={() => this.clickSessions()}>sessions</div></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="header-user-section">
                  {!loggedIn
                    ? <div className="logsignin">
                      <Link className={loginActive ? "logsigin__login-link active" : "logsigin__login-link"} to="/login">login / Join</Link>
                      <Link className={pricingActive ? "logsigin__signin-link pricing-link active" : "pricing-link logsigin__signin-link"} to="/pricing">Pricing</Link>
                      <div
                        className={"logsigin__shopCart-link cart-link " + (loggedIn ? '' : 'cart-link-unlogged')}
                        onClick={() => toggleCart(true)}
                      >
                        <div className="logsigin__shopCart-image"></div>
                        <div className="cartLength">{cartLength || null}</div>
                      </div>
                    </div>
                    : <div className="loggedin">
                      
                      <div className="loggedin-cart-aligner">
                        <div
                          className={"logsigin__shopCart-link cart-link " + (loggedIn ? '' : 'cart-link-unlogged')}
                          onClick={() => toggleCart(true)}
                        >
                          <div className="logsigin__shopCart-image"></div>
                          <div className="cartLength">{cartLength || null}</div>
                        </div>
                      </div>
                    </div>}
                </div>
              </div>
            </div>
            <div className="container-fluid mobile">
              <div className="row config--shadowoverlay">
                <div className="col-xs-6 logo">
                  <Link to="/">
                    <div className="logo__container-bgimage"></div>
                  </Link>
                </div>
                <div className="col-xs-6 action">
                  <a onClick={this.toggleSearch} className="action__search"><span className="screen-reader">action search</span><i className="fa fa-search" aria-hidden="true"></i></a>
                  
                  {!loggedIn && <Link to="/login" className="action__logsigin"><i className="fa fa-user" aria-hidden="true"></i></Link>}
                  <a className="action__logsigin" onClick={() => toggleCart(true)}><span className="screen-reader">shopping cart</span><i className="fa fa-shopping-cart" aria-hidden="true"></i></a>
                  <div className="action__togglemenu">
                    <button
                      className="action__openMenu"
                      onClick={() => this.toggleMobileMenu(mobileMenuOpen ? false : true)}>
                      {!mobileMenuOpen && <i className="fa fa-bars" aria-hidden="true"></i>}
                      {mobileMenuOpen && <i className="fa fa-times" aria-hidden="true"></i>}
                    </button>
                  </div>
                </div>
              </div>
              <MobileMenu
                logOut={this.logOut}
                open={mobileMenuOpen}
                loggedIn={loggedIn}
                toggleMobileMenu={this.toggleMobileMenu}
                hidePricing={agencyUser}
              />
              <MobileSearch 
                searchParam={searchParam}
                searchOptions={searchOptions}
                handleSearchParamChange={null}
                isOpenSearch={isOpenSearch}
                suggestions={this.state.suggestions}
                suggestInputChange={this.suggestInputChange}
                searchSubject={this.props.searchSubject}
                suggestAjaxInProgres={this.state.suggestAjaxInProgres}
                closeSearch={this.closeSearch}
                suggestionClick={this.suggestionClick}
                startSearch={this.startSearch}
              />
            </div>
          </div>
          <div className={"backlayout-overlay " + ((collectionsShown || sessionsShown || categoriesShown) ? "tab-open" : "")}
            onClick={() => {
              collectionsShown && this.closeCollectionsTab();
              categoriesShown && this.closeCategoriesTab();
              sessionsShown && this.closeSessionsTab();
            }}></div>
          <div
            className="categories-aligner"
            style={categoriesShown ? { overflow: 'hidden', height: categoriesHeight } : { overflow: 'hidden', height: 0 }}>
            {w > 767 ?
              <Categories
                categories={categories}
                open={categoriesShown}
                name="categories"
                close={() => this.closeCategoriesTab()}
                slideClick={(id, name) => this.handleSlideClick(id, name)}
              /> : null}
          </div>
          <div
            className="collections-aligner"
            style={sessionsShown ? { overflow: 'hidden', height: sliderHeight } : { overflow: 'hidden', height: 0 }}>
            {w > 767 ? <CollectionsSlider
              slides={sessions}
              open={sessionsShown}
              name="sessions"
              close={() => this.closeSessionsTab()}
              slideClick={(id, name) => this.handleSlideClick(id, name)}
            /> : null}
          </div>
          <div
            className="sessions-aligner"
            style={collectionsShown ? { overflow: 'hidden', height: sliderHeight } : { overflow: 'hidden', height: 0 }}>
            {w > 767 ? <CollectionsSlider
              slides={collections}
              open={collectionsShown}
              name="collections"
              close={() => this.closeCollectionsTab()}
              slideClick={(id, name) => this.handleSlideClick(id, name)}
            /> : null}
          </div>
        </header>
      </div>
    );
  }
}

export default withRouter(Header);

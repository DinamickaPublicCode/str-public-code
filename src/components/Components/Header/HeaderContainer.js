import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './Header';

import {
  toggleProductVisibility
} from '../../../actions/product';

import {
  toggleCart
} from '../../../actions/cart';

import {
  setSearchSubject
} from '../../../actions/gallery';

const mapStateToProps = ({ gallery, router, cart }) => ({
  searchSubject:gallery.get('searchSubject'),
  cart: cart.get('cart'),
  cartLength: cart.get('cartLength'),
  router: router,
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({
    setSearchSubject,
    toggleCart,
    toggleProductVisibility
  }, dispatch);

  export default connect(mapStateToProps, matchDispatchToProps)(Header);

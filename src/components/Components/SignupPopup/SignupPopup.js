import React, { Component } from 'react';
import './css/SignupPopup.css';
import Select from 'react-select';
import { AuthGroup, Interest } from '../';

let emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class SignupPopup extends Component {
  state = {
    interestOptions: [],
    selectedInterests: [],
    showInterestPage: false,
    data: {
      lastName: '',
      firstName: '',
      email: '',
      password: '',
      passConfirm: '',
      interest: ''
    }
  }

  handleChange = (key, val) => {
    let newData = Object.assign({}, this.state.data);
    newData[key] = val;

    this.setState({
      data: newData
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.shown !== nextProps.shown && !nextProps.productPageOpen) {
      if (nextProps.shown) {
        this.scrollOffset = window.scrollY;
        this.props.setScrollPosition(window.scrollY);
        window.$('body').css({ "overflow": "hidden", "position": "fixed", "marginTop": -window.scrollY });
        window.$('#root').css({ "overflow": "hidden", "position": "fixed", "marginTop": -window.scrollY });
      } else {
        window.$('body').css({ "overflow": "visible", "position": "static", "marginTop": 0 });
        window.$('#root').css({ "overflow": "visible", "position": "static", "marginTop": 0 });
        this.props.setScrollPosition(0);
        window.scrollTo(0, this.scrollOffset || 0);
      }
    }

    if (this.props.authMessage !== nextProps.authMessage && nextProps.authMessage) {
      this.props.displayMessage(nextProps.authMessage);
      if (nextProps.authMessage
        && (nextProps.authMessage.text === 'Confirmation email has been sent'
          || nextProps.authMessage.text === 'Success')) {
        this.props.toggleSignupPopup(false);
      }
    }
  }


  registerSocial = (interests) => {
    let { updateAuthMessage, registerSocial } = this.props;
    let requestdata = this.state.data;
    if (!requestdata || interests.length === 0) {
      updateAuthMessage({ type: 'error', text: 'Interests required!' })
      return;
    }

    let interestsConverted = interests.map(el => { return { id: el.id, name: el.name } });
    requestdata = Object.assign(requestdata, { interests: interestsConverted });
    registerSocial(requestdata)

  }

  signupWithData = (data) => {
    let { updateAuthMessage } = this.props;
    if (data.email === "") {
      updateAuthMessage({ type: 'error', text: 'Email required!' });
      return;
    }
    if (!emailRegex.test(data.email)) {
      updateAuthMessage({ type: 'error', text: 'Invalid email!' });
      return;
    }
    if (data.password === "") {
      updateAuthMessage({ type: 'error', text: 'Password required!' });
      return;
    }
    if (data.passConfirm !== data.password) {
      updateAuthMessage({ type: 'error', text: 'Passwords don\'t match!' });
      return;
    }
    if (!data.interest) {
      updateAuthMessage({ type: 'error', text: 'Please choose interests!' });
      return;
    }
  }

  clearSignupData = () => {
    this.setState({
      data: {
        lastName: '',
        firstName: '',
        email: '',
        password: '',
        passConfirm: '',
        interest: ''
      }
    })
  }

  signupForm = () => {
    return Object.keys(this.state.data).map((key, i) => {

      let isSelect = i === Object.keys(this.state.data).length - 1;

      let placeholder = key;
      if (key === 'firstname') placeholder = 'first name';
      else if (key === 'lastname') placeholder = 'last name';
      else if (key === 'passConfirm') placeholder = 'confirm password';

      let formItem = !isSelect ?
        <input
          className={"login-input"}
          value={this.state.data[key]}
          type={key === 'password' || key === 'passConfirm' ? 'password' : 'text'}
          placeholder={placeholder.toUpperCase()}
          onChange={(e) => this.handleChange(key, e.target.value)}
        />
        :
        <Select
          placeholder={'SELECT YOUR INTEREST'}
          onChange={(v) => this.handleChange(key, v)}
          value={this.state.data.interest}
          options={this.state.interestOptions}
          multi={true}
        />

      return (<div key={key} className={i === 0 || i === 1 ? "form-group input__double" : "form-group"}>
        {formItem}
      </div>)
    })
  }

  handleSubmit = () => {
    this.signupWithData(Object.assign({},
      this.state.data,
      { wantInfo: this.state.wantInfo })
    );
  }

  handleInterestChange = (v) => {
    this.setState({
      selectedInterests: v
    })
  }

  togglePopup = (e) => {
    if (e.target.className.includes('signup-popup-scroller')) {
      this.props.toggleSignupPopup(false);
    }
  }

  render() {
    let { shown } = this.props;
    let {
      interestOptions,
      showInterestPage,
      wantInfo,
      selectedInterests
    } = this.state;
    if (!shown) return null;

    return (
      <div className="signup-popup-wrapper">
        <div className="signup-popup-scroller" onClick={this.togglePopup}>
          <div className="join-container">
            <div className="login-container__form-wrapper">
              <div className="login-form">
                <h2 className="login-title"><span>CREATE ACCOUNT</span></h2>
                <form name="join" onSubmit={this.handleSubmit}>
                  <h1>{showInterestPage}</h1>
                  {showInterestPage
                    ? <Interest
                      interests={interestOptions}
                      finalizeSignup={this.registerSocial}
                      selectedInterests={selectedInterests}
                      onChange={this.handleInterestChange}
                    />
                    : this.signupForm()}
                </form>
                <div className="auth-button-group">
                  {showInterestPage ? null :
                    <AuthGroup
                      formKey={'signup'}
                      hasCheckbox={true}
                      checked={wantInfo ? true : false}
                      onCheckChange={() => this.setState({ wantInfo: !wantInfo })}
                      checkboxText={"I'd like to hear from Stock That Rocks"}
                      onClick={() => this.handleSubmit()}
                    />}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

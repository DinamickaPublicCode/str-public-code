import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SignupPopup from './SignupPopup';

import {
  toggleSignupPopup,
  updateAuthMessage
} from '../../../actions/auth';

import {
  displayMessage
} from '../../../actions/messaging';

import {
  setScrollPosition
} from '../../../actions/scroll';


const mapStateToProps = ({ auth, router, product }) => ({
  loggedIn: auth.get('loggedIn'),
  user: auth.get('user'),
  authMessage: auth.get('authMessage'),
  shown: auth.get('signupPopupShown'),
  productPageOpen: product.get('shown'),
  router: router
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({
    updateAuthMessage,
    displayMessage,
    toggleSignupPopup,
    setScrollPosition
  }, dispatch);


  export default connect(mapStateToProps, matchDispatchToProps)(SignupPopup);

import React, {Component} from 'react'

export default class NextArrow extends Component {
    render() {
        let {onClick} = this.props
        let {PUBLIC_URL} = process.env
        return <div className="slick-next slider-arrow" id="nextArrow" onClick={onClick}>
            <img src={`${PUBLIC_URL}/images/chevron-right.png`} alt="arrow" />
        </div>
    }
}

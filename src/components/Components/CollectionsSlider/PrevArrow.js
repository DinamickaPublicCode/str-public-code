import React, {Component} from 'react' 

export default class PrevArrow extends Component {
    render() {
        let {onClick} = this.props
        let {PUBLIC_URL} = process.env
        return <div className="slick-prev slider-arrow" id="prevArrow" onClick={onClick}>
            <img src={`${PUBLIC_URL}/images/chevron-left.png`} alt="arrow" />
        </div>
    }
}

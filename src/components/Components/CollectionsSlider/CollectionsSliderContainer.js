import React, { Component } from 'react'
import Slider from './Slider'
import { withRouter } from 'react-router-dom'
import './css/CollectionsSlider.css'

class CollectionsSlider extends Component {
	constructor() {
		super()
		this.state = {
			cls: 'closed',
			opacityCls: 'slider-hidden',
			showViewAll: true,
			sliderOpen: false,
			overlayDisplay: 'none',
			animationEnded: false
		}
		this.toggleViewAll = this.toggleViewAll.bind(this);
	}

	handleViewAll(name) {

		this.props.close()
		let _this = this;
		setTimeout(() => {
			_this.props.history.push('/' + name);
		}, 600)

	}

	toggleViewAll(slidesToShow) {

		if (this.props.slides.length <= slidesToShow) {
			this.setState({
				showViewAll: false
			})
		} else {
			this.setState({
				showViewAll: true
			})
		}
	}

	componentWillReceiveProps(nextProps) {

		if (nextProps.open && this.state.cls !== 'open') {
			this.setState({
				cls: 'open',
				opacityCls: 'slider-visible',
				sliderOpen: true
			})
			setTimeout(() => {
				this.setState({ animationEnded: true })
			}, 400)
		} else if (!nextProps.open && this.state.cls !== 'closed') {
			this.setState({
				cls: 'closed',
				opacityCls: 'slider-hidden',
				animationEnded: false
			}, () => {
				let _this = this;
				setTimeout(() => {
					_this.setState({
						sliderOpen: false
					})
				}, 600)
			})
		}
	}


	render() {
		let { slides, name, slideClick } = this.props
		let { cls, showViewAll, overlayDisplay, opacityCls, sliderOpen, animationEnded } = this.state

		return (<div className={'collections-slider-animation ' + cls}>
			<div className={"collections-slider-background " + opacityCls}>
				{(slides && slides.length > 0 && sliderOpen)
					? <Slider
						key={name}
						slideClick={slideClick}
						name={name}
						slides={slides}
						cls={cls}
						animationEnded={animationEnded}
						toggleViewAll={this.toggleViewAll}
					/>
					: <div className="no-slide-placeholder">
						{!slides || slides.length === 0 ? <span>No featured slides</span> : null}
					</div>}
			</div>
			<div className="view-all-btn-container">
				{showViewAll ? <div onClick={() => this.handleViewAll(name)} className="view-all-btn">
					View All
               </div> : null}
			</div>
			<div className="categories__backLayout" style={{ display: overlayDisplay }}></div>
		</div>)
	}
}

export default withRouter(CollectionsSlider)

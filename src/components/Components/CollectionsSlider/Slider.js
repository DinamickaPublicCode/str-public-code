import React, { Component } from 'react'
import Slick from 'react-slick'

import PrevArrow from './PrevArrow'
import NextArrow from './NextArrow'

export default class Slider extends Component {

	render() {
		let { slides } = this.props

		let settings = {
			dots: false,
			infinite: false,
			arrows: true,
			slidesToShow: 7,
			slidesToScroll: 1,
			prevArrow: <PrevArrow />,
			nextArrow: <NextArrow />,
			lazyLoad: true,
			responsive: [{
				breakpoint: 1650,
				settings: {
					slidesToShow: 6
				}
			}, {
				breakpoint: 1300,
				settings: {
					slidesToShow: 5
				}
			}, {
				breakpoint: 1150,
				settings: {
					slidesToShow: 4
				}
			}]
		}


		let { PUBLIC_URL } = process.env
		let { slideClick, name, animationEnded } = this.props
		return (
			<div className={!animationEnded && 'animated'}>
				<Slick {...settings} ref="slider" key={name}>
					{slides.length > 0 ? slides.map((el, i) => {

						let image = el.image || el.mainImage
						
						let imageStyle = {
							backgroundImage: `url(${image ? image.thumbUrl : (PUBLIC_URL + '/images/slide-placeholder.png')})`,
							backgroundSize: 'cover',
							border: image ? 'none' : '.5px solid rgb(91, 111, 106)'
						}

						return (
							<div className="slide collections-slide" key={`${i}-slide`}
								data-index={i}
								onClick={() => slideClick(el.id, name)}>
								<div className="header-slide-bg" style={imageStyle}></div>
								<div className="slide-title">{el.name || el.userSessionName}</div>
								<div className="slide-amount">
									<img src={`${PUBLIC_URL}/images/slides.png`} alt="slides" />
									<div>{el.totalProducts || 0}</div>
								</div>
							</div>
						)
					}) : <div></div>}
				</Slick>
			</div>

		)
	}
}

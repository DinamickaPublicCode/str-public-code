import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';

export default class AuthGroup extends Component {
    render() {
        let {
          hasCheckbox,
          onCheckChange,
          formKey,
          checked,
          checkboxText,
          onClick
        } = this.props
        let { PUBLIC_URL } = process.env;

        return (
          <div className="auth-group">
            <div className="auth-btn green" onClick={onClick}>{formKey === 'login' ? 'LOG IN' : 'SIGN UP'}</div>
            {hasCheckbox ? <div className="auth-checkbox-group">
                <span className="auth-checkbox-container"><input className="auth-checkbox" id="auth-check" type="checkbox" checked={checked} onChange={onCheckChange} />
                    <label htmlFor="auth-check">
                        <img src={PUBLIC_URL + '/images/check.png'} alt="check" />
                    </label>
                </span>
                <span className="auth-checkbox-text">{checkboxText}</span>
            </div> : null}
            <div className="or"><span>OR</span></div>
            <FacebookLogin
                appId=""
                fields="first_name,last_name,name,email,picture"
                textButton="CONNECT WITH FACEBOOK"
                cssClass="auth-btn facebook"
                isDisabled={true}
                />
            <GoogleLogin
                clientId="none"
                buttonText="CONNECT WITH GOOGLE"
                className="auth-btn google"
                disabled={true}
                onSuccess={() => null}
                onFailure={() => null}
            />
        </div>
      )
    }
}

import React, { Component } from 'react';
import './css/Cart.css';
import onClickOutside from "react-onclickoutside";
import { Scrollbars } from 'react-custom-scrollbars';
import { withRouter } from 'react-router-dom';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'

class Cart extends Component {
  state = {
    w: 0,
    h: 0,
    promoCode: 'PROMO CODE',
    promoCodeList: [],
    promoCodeListLocal: [],
    subTotal: 0
  }

  freezeElems = {};

  handleResize = () => {
    this.setState({ h: window.innerHeight, w: window.innerWidth})
  }

  handleClickOutside = () => {
    let { loginPopupShown, signupPopupShown } = this.props;
    if (!loginPopupShown && !signupPopupShown) {
      this.props.toggleCart(false);
    }
  }

  handlePromoChange = (tags) => {
    this.setState({
      promoCodeListLocal: tags
    });
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    this.handleResize();
    this.props.createAnonymousCart()
  }

  addToFaves = () => {
    let {
      loggedIn,
      toggleLoginPopup,
    } = this.props;

    if (!loggedIn) {
      toggleLoginPopup(true);
      return;
    }
  }

  removeProductFromCart = (index) => {
    let { removeFromCart, cart, loggedIn } = this.props;

    let newCart = Object.assign({}, cart);
    let removedProduct = Object.assign({}, newCart.products[index]);
    newCart.products.splice(index, 1);
    removeFromCart(newCart, !loggedIn, removedProduct.id);
  }

  removePromoCodeFromCart = (index) => {
    let { updateCartContent, cart, loggedIn } = this.props;

    let newCart = Object.assign({}, cart);
    newCart.promoCodeList.splice(index, 1);
    updateCartContent(newCart, !loggedIn);
  }

  viewCartProduct = (id, i) => {
    let {
      selectProductId,
      changeProductSource,
      toggleProductVisibility
    } = this.props;

    changeProductSource(true, i);
    selectProductId(id, i);
    toggleProductVisibility(true);
  }

  onPromocodeChange = (e) => {
    this.setState({ promoCode: e.target.value });
  }

  applyPromocode = () => {

    let { promoCodeListLocal } = this.state;

    let { cart, updateCartContent, loggedIn } = this.props;

    let newCart = Object.assign({}, cart);

    if (newCart['promoCodeList'] && newCart['promoCodeList'].length > 0) {
      // check if promocode to be applied is already "in cart"
      let promosToAdd = promoCodeListLocal.filter((el) => {
        return newCart['promoCodeList'].indexOf(el) === -1;
      });

      newCart['promoCodeList'] = newCart['promoCodeList'].concat(promosToAdd);
    } else {
      newCart['promoCodeList'] = promoCodeListLocal;
    }

    updateCartContent(newCart, !loggedIn);
    this.setState({
      promoCodeListLocal: []
    });
  }

  confirmPurchase = () => {
    let {
      loggedIn,
      toggleCart
    } = this.props;

    toggleCart(false);

    if (!loggedIn) {
      this.props.history.push('/login');
    } else {
      this.props.history.push('/checkout');
    }
  }

  renderCartProduct = (el, i) => {
    let price = el.prices.filter(item => el.size === item.type)[0].price;
    el.prices.forEach((elem => {
      if (elem.type === 'MARKET_FREEZE') {
        this.freezeElems[el.id] = true;
      }
    }))

    return (

      <div className="cart-item" key={i}>
        <div className="cart-item-image" onClick={() => this.viewCartProduct(el.id, i)}>
          <div style={{backgroundImage: `url(${el.image.thumbUrl})`}}></div>
        </div>
        <div className="cart-item-info">
          <div className="cart-item-info-left">
            <div className="cart-item-title">
              {`ITEM: ${el.id}`}
            </div>
            <div className="cart-item-size">
              <div>{el.size + ' SIZE'}</div>
              {this.freezeElems[el.id] && <div>JUST FOR YOU</div>}
              {el.extendedLicense && !this.freezeElems[el.id] && <div>EXTENDED LICENSE</div>}
            </div>
            
            <div className="cart-item-faves" onClick={() => this.addToFaves(el, i)}>
              <i className="fa fa-heart-o"></i>
              <span> Add to favorites</span>
            </div>
              
          </div>
          <div className="cart-item-info-right">
              <div className="cart-item-price">
                {`$${price} `}
                <i className="fa fa-trash-o" onClick={() => this.removeProductFromCart(i)}></i>
              </div>
          </div>
        </div>
        <hr />
      </div>
    );
  }

  renderPromoCode = (el, i) => {
    let { cart } = this.props;
    let cartHasItems = (
      (cart.products && cart.products.length > 0) ||
      (cart.packages && cart.packages.length > 0) ||
      cart.membership
    ) ? true : false;

    return (
      <div className="cart-item cart-item-package" key={i}>
        {(cartHasItems || i > 0) && <hr />}
        <div className="cart-item-placeholder"></div>
        <div className="cart-item-info">
          <div className="cart-item-info-left">
          <div className="cart-item-title">
              {el}
            </div>
            <div className="cart-item-size">
               PROMO CODE
            </div>
          </div>
          <div className="cart-item-info-right">
              <div className="cart-item-price">
                <i className="fa fa-trash-o" onClick={() => this.removePromoCodeFromCart(i)}></i>
              </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    let { open = false, toggleCart, cart } = this.props;
    let { h, promoCodeListLocal } = this.state;

    this.freezeElems = {};

    let scrollHeight = h - 320;
    if (!cart) return null;

    return (
      <div className={"cart-sidebar cart-wrapper " + (open ? 'open' : '')}>
        <div className="cart-content">
          <div className="cart-top">
            <div className="cart-title">SHOPPING CART</div>
          </div>
          <div className="cart-scroller">
            <Scrollbars autoHide style={{ height: scrollHeight }}>
                {cart.products && <div>
                  {cart.products.length > 0 && cart.products.map(this.renderCartProduct)}
                </div>}
                {cart.promoCodeList && <div>
                  {cart.promoCodeList && cart.promoCodeList.length > 0 && cart.promoCodeList.map(this.renderPromoCode)}
                </div>}
            </Scrollbars>
          </div>
          <div className="cart-bottom">
            <div className="promo-code-wrapper">
              <div className="promo-code-input-group">
               <TagsInput
                 value={promoCodeListLocal}
                 addKeys={[32, 13]}
                 addOnPaste={true}
                 addOnBlur={true}
                 inputProps={
                   {
                     className: 'react-tagsinput-input promo-taginput',
                     placeholder: 'PROMO CODE'
                  }
                 }
                 onChange={this.handlePromoChange} />
                <button className="promo-button" onClick={this.applyPromocode}>APPLY</button>
              </div>
            </div>
            <div className="subtotal-wrapper">
              <div className="subtotal-aligner">
                <div>SUBTOTAL</div>
                <div>{`$${cart.totalPrice && cart.totalPrice.toFixed(2)}`}</div>
              </div>
            </div>
            <div className="checkout-wrapper">
              <button className="checkout-btn" onClick={this.confirmPurchase}>CHECKOUT</button>
            </div>
          </div>
          <div className="close-cart" onClick={() => toggleCart(false)}></div>
        </div>
      </div>
    );
  };
};

export default withRouter(onClickOutside(Cart))

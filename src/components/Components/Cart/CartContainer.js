import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Cart from './Cart';

import {
  updateCartContent,
  toggleCart,
  removeFromCart,
  createAnonymousCart
} from '../../../actions/cart';

import {
  changeProductSource,
  selectProductId,
  toggleProductVisibility
} from '../../../actions/product';

import {
  toggleLoginPopup
} from '../../../actions/auth';

import {
  displayMessage
} from '../../../actions/messaging';

const mapStateToProps = ({ auth, cart, router }) => ({
  loggedIn: auth.get('loggedIn'),
  cart: cart.get('cart'),
  open: cart.get('open'),
  loginPopupShown: auth.get('loginPopupShown'),
  signupPopupShown: auth.get('signupPopupShown'),
  router: router
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({
    updateCartContent,
    toggleCart,
    changeProductSource,
    selectProductId,
    toggleProductVisibility,
    removeFromCart,
    toggleLoginPopup,
    displayMessage,
    createAnonymousCart
  }, dispatch);

  export default connect(mapStateToProps, matchDispatchToProps)(Cart);

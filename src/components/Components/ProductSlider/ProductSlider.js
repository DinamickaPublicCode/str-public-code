import React, { Component } from 'react';
import './css/ProductSlider.css';
import { Link } from 'react-router-dom';

export default class ProductSlider extends Component {


  componentDidMount() {
    this.initSwiper();
  }

  initSwiper = () => {
    let { type, isVertical } = this.props;
    var mySwiper = new window.Swiper('.swiper-container-' + this.props.type, {
      speed: 400,
      spaceBetween: 15,
      slidesPerView: isVertical ? 4 : 5,
      loop: false,
      loopAdditionalSlides: 10,
      navigation: {
        prevEl: `.${type}-arrow-prev`,
        nextEl: `.${type}-arrow-next`
      },
      breakpoints: {
        1200: {
          slidesPerView: 4
        },
        900: {
          slidesPerView: 3
        },
        767: {
          slidesPerView: 2
        },
        500: {
          slidesPerView: 1
        }
      }
    });

    this.swiper = mySwiper;

    // fallback for duplicated slides
    this.swiper.on('click', (e) => {
      let { imageClick = null, pageMode = false } = this.props;
      if (e.target.className.includes('swiper-slide') || e.target.className.includes('swiper-img')) {
        let id = e.target.dataset.id;
        if (imageClick && !pageMode) imageClick(id);
      }
    });

  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.slides !== this.props.slides && this.swiper) {
      //this.swiper.update();
      this.swiper.destroy();
      this.initSwiper();
    }
  }

  renderSlide = (el, i) => {
    let { type, imageClick = null, pageMode = false, activeProduct = null } = this.props;

    if (activeProduct && activeProduct.id === el.id) return null;
    // if (!el || !el.image || !el.image.thumbUrl) return null;
    let { PUBLIC_URL } = process.env;

    let imageUrl = (el && el.image && el.image.thumbUrl) ? el.image.thumbUrl : PUBLIC_URL + '/images/slide-placeholder.png';

    let url  = type === 'sessions' ? 'product' : 'collections';
    return (
      <div className="swiper-slide" data-id={el.id} key={i} onClick={() => {
        if (imageClick && !pageMode) imageClick(el.id);
      }}>
        {
          (type === 'sessions' && !pageMode)
          ? (
            <div style={{'cursor': 'pointer'}}>
              <div className="swiper-img" data-id={el.id} style={{backgroundImage: `url(${imageUrl})`}} />
              {type === 'collections' && <div className="dashboard-slide__label">{el.name}</div>}
            </div>
          )
          : (<Link to={`../${url}/${el.id}`}>
            <div className="swiper-img" data-id={el.id} style={{backgroundImage: `url(${imageUrl})`}} />
            {type === 'collections' && <div className="dashboard-slide__label">{el.name}</div>}
          </Link>
          )
        }
      </div>
    )
  }
  render() {
    let { slides = [], type, sliderLoading = false, isSmall = false } = this.props;
    let swiperSlides = slides.map(this.renderSlide);
    let { PUBLIC_URL } = process.env;
    return (
        <div className={"swiper-container-" + type}
          style={{ opacity: sliderLoading ? 0 : 1 }}
        >
          <div className="swiper-wrapper">
            {swiperSlides}
          </div>
          {swiperSlides.length > 0 && <div className={`arrow-prev ${type}-arrow-prev`}>
              {!isSmall && <img src={`${PUBLIC_URL}/images/chevron-left-gray.png`} alt="arrow" />}
              {isSmall && <i className="fa fa-angle-left"></i>}
          </div>}
         {swiperSlides.length > 0 && <div className={`arrow-next ${type}-arrow-next`}>
              {!isSmall && <img src={`${PUBLIC_URL}/images/chevron-right-gray.png`} alt="arrow" />}
              {isSmall && <i className="fa fa-angle-right"></i>}
          </div>}
        </div>
    );
  }
}

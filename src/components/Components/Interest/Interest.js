import React, {Component} from 'react'
import Select from 'react-select'
import './css/Interest.css'

export default class Interest extends Component {
  render() {
    let {interests, finalizeSignup, selectedInterests, onChange} = this.props
    return (
      <div className="interest-wrapper">
        <div className="interest-title">
          <h1 className="interest-title-text">Select interest</h1>
        </div>
        <div className="interest-dropdown">
        <Select
            placeholder={'SELECT YOUR INTEREST'}
            onChange={(v) => onChange(v)}
            value={selectedInterests}
            options={interests}
            multi={true}
        />
        </div>
        <div className="auth-btn-container">
          <div className="auth-btn green" onClick={() => finalizeSignup(selectedInterests)}>DONE</div>
        </div>
      </div>
    )
  }
}

import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom'
import './css/Footer.css';


class Footer extends Component {
	render() {
		let path = this.props.history.location.pathname
		let footerShown = true
		if (path.includes('auth')
			|| path.includes('newpass')
			|| path.includes('forgot-password')) {
			footerShown = false
		}
		return footerShown ? (<footer>
			<div className="container-fluid footer-body">
				<div className="row footer-flex-aligner">
					<div className="col-xs-12 logo">
						<Link to="/" className="logo_link">
							<div className="logo__container-bgimage"></div>
						</Link>
						<div className="newletter-sign">
							<p className="newletter-sign__title-txt">Newsletter signup</p>
							<form action="" className="newsletter" onSubmit={e => e.preventDefault()}>
								<div className="newsletter__inputgroup">
									<input type="text" className="newsletter__input" placeholder="Your Email" />
									<button type="submit" className="newsletter__submit"></button>
								</div>
							</form>
						</div>
					</div>
					<div className="col-xs-12 footer-learnmore">
						<p className="footerMenu__title">Learn more</p>
						<ul className="footerMenu__menu">
							<li className="footerMenu__menuItem"><Link to="/pricing">Prices</Link></li>
							<li className="footerMenu__menuItem"><Link to="/pricing/memberships">Membership</Link></li>
							<li className="footerMenu__menuItem"><Link to="/faq">FAQ</Link></li>
						</ul>
					</div>
					<div className="col-xs-12">
						<p className="footerMenu__title">LEGAL INFORMATION</p>
						<ul className="footerMenu__menu">
							<li className="footerMenu__menuItem"><Link to="/legal-information/licensing">Photo licensing</Link></li>
							<li className="footerMenu__menuItem"><Link to="/showcase">How to use</Link> </li>
							<li className="footerMenu__menuItem"><Link to="/legal-information/terms-policy">Terms Policy</Link></li>
							<li className="footerMenu__menuItem"><Link to="/legal-information/privacy-policy">Privacy Policy</Link></li>
						</ul>
					</div>
					<div className="col-xs-12">
						<p className="footerMenu__title">Company & Support</p>
						<ul className="footerMenu__menu">
							<li className="footerMenu__menuItem"><Link to="/about">About</Link></li>
							<li className="footerMenu__menuItem"><Link to="/contact">Contact</Link></li>
							<li className="footerMenu__menuItem"><Link to="/blog">Blog</Link></li>
						</ul>
					</div>
					<div className="col-xs-12">
						<p className="footerMenu__title">Follow us</p>
						<div className="social">
							<div className="social__item"><a href="https://www.instagram.com/"><span className="screen-reader">instagram</span><i className="fa fa-instagram" aria-hidden="true"></i></a></div>
							<div className="social__item"><a href="https://www.pinterest.com/"><span className="screen-reader">pinterest</span><i className="fa fa-pinterest-p" aria-hidden="true"></i></a></div>
							<div className="social__item"><a href="https://www.facebook.com/"><span className="screen-reader">facebook</span><i className="fa fa-facebook" aria-hidden="true"></i></a></div>
						</div>
					</div>
				</div>
			</div>
			<div className="copyright">
				<p>© 2017 stock that rocks</p>
			</div>
		</footer>) : <div></div>
	}
}

export default withRouter(Footer);

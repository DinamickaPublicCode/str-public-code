import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Footer from './Footer';

const mapStateToProps = ({ product }) => ({
  productPageOpen: product.get('shown') 
});

const matchDispatchToProps = dispatch =>
  bindActionCreators({

  }, dispatch)

export default connect(mapStateToProps, matchDispatchToProps)(Footer);

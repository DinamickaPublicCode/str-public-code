import React, { Component } from 'react';
import { Switch } from 'react-router-dom'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers/root';

import {
  Home,
  Product,
  NotFound
} from './pageInjector'

import createHistory from 'history/createBrowserHistory'
import { Route } from 'react-router'

import { ConnectedRouter, routerMiddleware } from 'react-router-redux'

import {
  Header,
  Footer,
  Cart,
  MessagingGlobal,
  EmailSharePopup,
  LoginPopup,
  SignupPopup
 } from './components/Components'

const history = createHistory()
const routeMiddleware = routerMiddleware(history)

let store = createStore(rootReducer,
   composeWithDevTools(applyMiddleware(thunkMiddleware, routeMiddleware)));

class Container extends Component {

  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>

          <div className="page-wrapper">
            <Header />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/collections/:id" component={Home} />
              <Route exact path="/sessions/:id" component={Home} />
              <Route exact path="/categories/:id" component={Home} />
              <Route exact path="/search"render={() => <Home search={true}/>}  />
              <Route render={() => <NotFound />} />
            </Switch>

            <Product />
            <Footer />
            <Cart />
            <EmailSharePopup />
            <MessagingGlobal global={true} />
            <LoginPopup />
            <SignupPopup />
            
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default Container;
